from jinja2 import Environment, FileSystemLoader, Template
import glob
import os
from itertools import combinations
from tools.waterclusters import make_full_idx

def makedir(dirpath):
    try: 
        os.makedirs(dirpath)
    except OSError:
        if not os.path.isdir(dirpath):
            raise

class Templater(object):
    ''' Input script generator based on jinja2 templating. 
   
        Current limitations:
            - The setting of paths is rigid, bad, and probably not 
              robust whatsover. 
            - Only QM/MM templates, no QM/QM or MM/MM

        
        Currently supported DFT programs:
            - GPAW
            - CRYSTAL
            - ORCA

        Currently supported calculation types:
            - WaterClusters: Interaction energy differences of all 
                             QM/MM permutations of a large dataset.

                             See e.g. doi:10.1021/acs.jctc.9b00778

            - PES:           Potential (interaction) Energy Surface
                             scans of the water dimer. 
                             Currently:
                                roo, alpha, ooh, hoox, ooxh
                            
                             See: doi:10.1039/c3cp52097h for defs. 

                             It is however easy to add more, just add
                             geometry creators to ./tools/pesmaker.py
                             keeping the input of set_<geom> the same
                             as the already existing ones. 

            - Geometry:      Geometry for all QM/MM Permutations of a
                             set of n-mers. E.g. Hexamers. 
                             See: doi:10.1021/acs.jctc.9b00778
                             
         '''

    def __init__(self, megadict=None, templatedir='./TEMPLATES/',
                 program='gpaw', mode='grid', calctype='WaterClusters',
                 h=0.15, pestype=None, basis=None, extra_tag=''):
            self.megadict = megadict
            self.templatedir = templatedir
            self.program = program
            self.rootdir = os.getcwd()
            self.calctype = calctype
            self.tfile = None
            self.mode = mode
            self.pestype = pestype
            self.extra_tag = extra_tag
            self.h = h
            self.basis = basis

            self.orcablocks = None


    def set_templatefile(self, custom_tfile=None):
        # XXX Might be easiest just to set this manually..
        if self.calctype == 'WaterClusters' and self.program == 'gpaw':
            tfile = 't_gpaw_grid.py'
        elif self.calctype == 'WaterClusters' and self.program == 'crystal':
            tfile = 't_crystal_clusters.py'
        elif self.calctype == 'PES' and self.program == 'crystal':
            self.tfile = 't_crystal_pes.py'
        elif self.calctype == 'PES':
            tfile = 't_gpaw_pes.py'
        elif self.calctype == 'PES' and self.program == 'orca':
            tfile = 't_orca_pes.py'
        elif self.calctype.lower() == 'geometry' and self.program == 'gpaw':
            tfile = 't_gpaw_qmmm_geometry.py'

        if not tfile:
            print('Warning: No template found, things will probably break')

        if custom_tfile is not None:
            tfile = custom_tfile

        self.tfile = tfile

        return tfile


    def set_paths(self):  # there must be a prettier way of doing this
        if self.megadict['extrapath'] and not self.pestype:
            xtra = self.megadict['extrapath']+'/'
            self.inpdir = './RUNS/'+self.calctype+'/'+self.program+\
                          '/'+xtra+self.xc+'/'+self.mode+'/'

        elif self.pestype is not None and self.megadict['extrapath']:
            xtra = self.megadict['extrapath']+'/'
            self.inpdir = './RUNS/'+self.calctype+'_'+self.pestype+\
                           '/' +self.program+'/'+xtra+self.xc+'/'+self.mode+'/'
        elif self.pestype is not None:
            self.inpdir = './RUNS/'+self.calctype+'_'+self.pestype+\
                           '/' +self.program+'/'+self.xc+'/'+self.mode+'/'

        else:
            xtra = self.megadict['extrapath']+'/'
            self.inpdir = './RUNS/'+self.calctype+'/'+self.program+\
                          '/'+xtra+self.xc+'/'+self.mode+'/'
        self.xyzdir = '/XYZs/'


    def set_defaults(self, xc='PBE', mmcalc=None, mmcalc_args='', lj=None, 
                     lj_custom=None):

        default_points = {'roo': 'np.hstack((np.arange(2.2, 3.05, 0.05)\n,\
                                  np.arange(3.1, 5.1, 0.1)\n,\
                                  np.arange(6.0, 12, 1.)))',
                          'alpha':'np.arange(-82., 84., 2)',
                          'ooh': 'np.arange(-54., 56., 2)',
                          'hoox': 'np.arange(-150., 155., 5)',
                          'ooxh': 'np.arange(-70., 75., 5)'}

        # default hexamer values
        fmax = 0.02
        geom_steps = 200 

        if self.program == 'gpaw':
            self.xc = xc
            if self.mode == 'grid':
                gpaw_args = {'eigensolver': 'RMMDIIS(niter=5)', 
                             'h': self.h,
                             'xc': self.xc}
            elif self.mode == 'lcao':
                basis = self.basis
                if basis is None:
                    basis = 'tzp'
                gpaw_args = {'h': self.h,
                             'xc': self.xc,
                             'mode': 'lcao',
                             'basis': basis}

            convergence = {'density': 5e-5, 
                           'eigenstates': 4e-8}


            gpaw_args['convergence'] = convergence

            scme_param = {'damping_value': 0.755,
                          'buffer_region': 15.0}

            interaction = 'Calc_QMSCME()'
            embedding = 'SCMEEmbedding(**parameters)'
            if mmcalc is None:
                mmcalc = 'SCME_Interface(mmatoms, qmmm=True)'
            if 'TIP' in mmcalc:
                n = mmcalc.split('TIP')[1][0]
                interaction = 'LJInteractions({(\'O\', \'O\'): (eps' + n + ', sig' + n + ')})'
                embedding = 'Embedding()'
            
            pure_mmcalc = mmcalc
            if 'SCME_Interface' in pure_mmcalc:
                pure_mmcalc = 'SCME_Interface(mmatoms)'
                convergence['dpoles'] = '1e-7' 
                convergence['qpoles'] = '1e-7' 

            megadict = {'gpaw_args': gpaw_args, 
                         'convergence': convergence, 
                         'scme_param': scme_param,
                         'vac': 7.0, 
                         'interaction': interaction,
                         'embedding': embedding,
                         'mmcalc': mmcalc,
                         'mmcalc_args':mmcalc_args,
                         'pure_mmcalc': pure_mmcalc,
                         'rootdir': self.rootdir + os.sep + 'tools',
                         'qmpbc': False,
                         'extrapath': '',
                         'qmidx': None,
                         'fmax': fmax,
                         'geom_steps': geom_steps}
            
        elif self.program == 'crystal':
            self.xc = xc
            self.mode = '' # no grid/lcao mode. 
            cryst_args = {'label':'h2o', 'xc':self.xc, 
                          'guess':True, 'oldgrid':True, 
                          'otherkeys':['anderson', ['toldee', '6']]}

            # Lennard Jones options
            if lj_custom is None:
                interaction = 'LJInteractions({(\'O\', \'O\'): (epsilon0, sigma0)})'
            
            # The PES script has 3 pre-made LJ options:
            # int_b3lyp, int_tip3p, int_tip4p
            # should be able to choose via the MM calc choice
            if mmcalc is not None:
                if 'tip' in mmcalc.lower() and lj is None:
                    lj = 'int_' + mmcalc.lower().split('()')[0]
            
            if mmcalc is None and lj is None:
                lj = 'int_tip3p'
            
            if mmcalc is None:
                mmcalc = 'TIP3P'
                mmcalc_args = ''
            
            megadict = {'cryst_args': cryst_args, 
                         'vac': 0.1, 
                         'interaction' : interaction,
                         'lj': lj, 
                         'mmcalc': mmcalc,
                         'mmcalc_args': mmcalc_args,
                         'rootdir': self.rootdir + os.sep + 'tools',
                         'qmpbc': False,
                         'extrapath': '',
                         'extra_tag': self.extra_tag}

        elif self.program == 'orca':
            if self.basis is None:
                self.basis = 'def2-SVP'

            self.xc = xc.upper()
            self.mode = ''
            osi = self.xc + ' ' + self.basis
            ob = self.orcablocks
            orca_args = {'orcasimpleinput':osi, 
                         'orcablocks': ob}

            if mmcalc is None:
                mmcalc = 'TIP4P'
                mmcalc_args = '(rc=100.)'

            #QM/MM Defaults
            # Lennard Jones options
            if lj_custom is None:
                if 'TIP' in mmcalc:
                    n = mmcalc.split('TIP')[1][0]
                    interaction = 'LJInteractions({(\'O\', \'O\'): (eps' + n + ', sig' + n + ')})'
            else:
                interaction = lj_custom

            

            megadict = {'orca_args': orca_args,
                        'xc': self.xc,
                        'basis':self.basis,
                        'vac' : 0.1, 
                        'interaction': interaction, 
                        'mmcalc': mmcalc,  
                        'mmcalc_args': mmcalc_args,
                        'rootdir': self.rootdir + os.sep + 'tools',
                        'qmpbc': False,
                        'extrapath': '',
                        'embedding': 'Embedding()',
                        'extra_tag': self.extra_tag,
                         'fmax': fmax,
                         'geom_steps': geom_steps}



        if (self.calctype == 'PES') and self.pestype is None:
            raise ValueError('Must specify which PES to generate for PES runs')
        elif self.calctype == 'PES':
            megadict['points'] = default_points[self.pestype]
            megadict['geom'] = self.pestype

        self.megadict = megadict


    def set_parameter(self, parameter, value):
        self.megadict[parameter] = value 


    def get_structurefiles(self, fpath=None):
        if fpath is None:
            fpath = self.rootdir+self.xyzdir
        
        self.sfiles = sorted(glob.glob(fpath + '*xyz'))
        if self.calctype.lower() == 'geometry':
            # Standard for Geometry is only BT Hexamers
            self.sfiles = sorted(glob.glob(fpath + 'BT*xyz'))

        if len(self.sfiles) == 0:
            raise EnvironmentError('No xyz files!')


    def fill_script(self, xyzfile=None):
        env = Environment(loader=FileSystemLoader(self.templatedir))
        template = env.get_template(self.tfile)
        script = template.render(xyzfile=xyzfile, **self.megadict)
        lines = script.split('\n')

        return lines


    def write_inputscript(self, lines, f, extra=''):
        f = f.split('/')[-1]  # get xyzfile name only
        if '.xyz' in f:
            f = f.split('.xyz')[0]
        if self.calctype == 'PES':  # dont name after xyzfile, name after pes
            f = self.pestype
            ff = open(self.inpdir + f + extra + '.py', 'w') 
            
        else:
            ff = open(self.inpdir + f + extra + '.py', 'w') 
        for l in lines:
            ff.write(l)
            ff.write('\n')
        ff.close()


    def make_qmmm_combis(self, xyzfile):
        with open(xyzfile, 'r') as f:
            numatms = int(f.readline())
        nummols = numatms // 3
        return make_full_idx(nummols)
  

    def make(self, structurefile=None, extra=''):
        if self.tfile == None:
            self.tfile = self.set_templatefile()

        if self.megadict == None:
            self.set_defaults()

        self.set_paths()

        if structurefile == None:
            self.get_structurefiles()
        else:
            self.sfiles = [structurefile]

        makedir(self.inpdir)
        makedir(self.inpdir+'/outputfiles')

        rundir = self.rootdir + self.inpdir[1:]
        self.megadict['rundir'] = rundir

        for f in self.sfiles:
            print(f)
            combis = [0]  # to do 1 iter for all other calctypes 
            if self.calctype.lower() == 'pes' and 'qmidx' in self.megadict.keys():
                combis = [self.megadict['qmidx']]
            if self.calctype.lower() == 'geometry' and 'qmqm' not in self.tfile:
                combis = self.make_qmmm_combis(f)
            for c, combi in enumerate(combis):
                self.megadict['qmidx'] = combi
                extra = '_{0:03d}'.format(c)
                self.megadict['extra'] = extra
                lines = self.fill_script(f)
                self.write_inputscript(lines, f, extra=extra)


""" CRYSTAL def2-TZVP basis file. Needs to be written to ./basis. somehow
1 4
0 0 3 1.0 1.0
34.061341000 0.00602519780
5.1235746000 0.04502109400
1.1646626000 0.20189726000
0 0 1 0.0 1.0
0.4157455100 1.00000000000
0 0 1 0.0 1.0
0.1795111000 1.00000000000
0 2 1 0.0 1.0
0.8000000000 1.00000000000
8 9
0 0 6 2. 1.
  27032.382631      .21726302465E-03
  4052.3871392      .16838662199E-02
  922.32722710      .87395616265E-02
  261.24070989      .35239968808E-01
  85.354641351      .11153519115
  31.035035245      .25588953961
0 0 2 2. 1.
  12.260860728      .39768730901
  4.9987076005      .24627849430
0 0 1 0. 1.
  1.1703108158      1.0000000000
0 0 1 0. 1.
  .46474740994      1.0000000000
0 0 1 0. 1.
  .18504536357      1.0000000000
0 2 4 4. 1.
  63.274954801      .60685103418E-02
  14.627049379      .41912575824E-01
  4.4501223456      .16153841088
  1.5275799647      .35706951311
0 2 1 0. 1.
  .52935117943      .44794207502
0 2 1 0. 1.
  .17478421270      .24446069663
0 3 1 0. 1.
  1.20   1.00
1000 4
0 0 3 0.0 1.0
34.061341000 0.00602519780
5.1235746000 0.04502109400
1.1646626000 0.20189726000
0 0 1 0.0 1.0
0.4157455100 1.00000000000
0 0 1 0.0 1.0
0.1795111000 1.00000000000
0 2 1 0.0 1.0
0.8000000000 1.00000000000
100 9
0 0 6 0. 1.
  27032.382631      .21726302465E-03
  4052.3871392      .16838662199E-02
  922.32722710      .87395616265E-02
  261.24070989      .35239968808E-01
  85.354641351      .11153519115
  31.035035245      .25588953961
0 0 2 0. 1.
  12.260860728      .39768730901
  4.9987076005      .24627849430
0 0 1 0. 1.
  1.1703108158      1.0000000000
0 0 1 0. 1.
  .46474740994      1.0000000000
0 0 1 0. 1.
  .18504536357      1.0000000000
0 2 4 0. 1.
  63.274954801      .60685103418E-02
  14.627049379      .41912575824E-01
  4.4501223456      .16153841088
  1.5275799647      .35706951311
0 2 1 0. 1.
  .52935117943      .44794207502
0 2 1 0. 1.
  .17478421270      .24446069663
0 3 1 0. 1.
  1.20   1.00
"""
