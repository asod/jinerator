from jinerate import Templater
import numpy as np

t = Templater(calctype='PES', pestype='roo', h=0.15)
t.set_defaults(xc='RPBE')
t.set_paths()
t.megadict['vac'] = 7.
gs = np.linspace(0.755-.25, 0.755+.25, 20)
for g in gs:
    t.megadict['scme_param']['damping_value'] = g
    for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('_RPBESCMEg{0:0.4f}'.format(g), '_SCMERPBEg{0:0.4f}'.format(g) )):
        t.megadict['qmidx'] = qmidx
        t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra=conf)
