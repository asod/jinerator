from jinerate import Templater
import numpy as np

basis={'H': 'Def2-TZVPPD.sz',
       'O': 'Def2-TZVPPD.sz'}

t = Templater(calctype='PES', pestype='roo', h=0.18, mode='lcao')
t.set_defaults(xc='PBE')
t.megadict['gpaw_args']['basis'] = basis
t.megadict['extrapath'] = 'def2-TZVPPD'
t.set_paths()
t.megadict['vac'] = 4.
gs = np.linspace(0.755-.5, 0.755+.5, 15)
for g in gs:
    t.megadict['scme_param']['damping_value'] = g
    for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('_PBEdef2SCMEg{0:0.4f}'.format(g), '_SCMEdef2PBEg{0:0.4f}'.format(g))):
        t.megadict['qmidx'] = qmidx
        t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra=conf)
