from jinerate import Templater

t = Templater(calctype='WaterClusters', program='orca', mode='lcao', basis='def2-TZVP')
t.orcablocks = '%scf Convergence verytight \nmaxiter 300 end\n\n%pal nprocs 16 end\n\n'
t.set_defaults(xc='BLYP')
t.tfile = 't_orca_clusters.py'
t.set_paths()
t.make()
