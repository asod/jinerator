import glob
from matplotlib import pyplot as plt
from tools.npy_parser import NPYParser
from tools.violinplot import make_clusterplot

# input as directory containing npy files from WaterClustrers benchmark,
# or list of npy-files, as here (such that it wont also read the 
# *qm_monos.npy files ). This way you can also look closer at a sub-
# selection of the clusters (e.g. the Bates & Tschumper hexamers):
fil = sorted(glob.glob('/home/asod/Dropbox/HI/ORCAQMMM/BENCHMARK/RUNS/RUNS_RAN/WaterClusters/orca/def2-SVP/BLYP/BT*_.npy'))
nocp = NPYParser(fil)
functions = [nocp.delta_eint]  # -analysis functions to use on the dataset. Many of them are SCME-specific.
outnames = ['alldeints', 'DDE']
nocp.get_data(functions=functions, outnames=outnames)
fig, ax = plt.subplots(1,1, figsize=(12,8));
xbars, patches = make_clusterplot(ax, nocp)
legend = ax.legend([patches[0], ], 
                        ['Max Single-Model Difference', ],
                        loc='upper left',
                        labelspacing=-0.1)
plt.savefig('example_violins.pdf', bbox_inches='tight', dpi=300) 
