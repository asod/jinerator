""" Run from root dir with mpirun -np X python -m EXAMPLES/par_adf """

from tools.adf import ADF
import glob
from mpi4py import MPI

world = MPI.COMM_WORLD

trajs = sorted(glob.glob('/home/asod/Dropbox/HI/SCME/2018_May/Droplet_MD/DropletSCME_RDF/fromGarpur/*.traj'))
 
trajs = [t for t in trajs if 'aligned' not in t]
mmmm_adf = ADF(trajs, [0], pbc=False, world=world)
mmmm_adf.run_traj_parallel()

trajs = sorted(glob.glob('/home/asod/Dropbox/HI/SCME/2018_September/liquidPBETIP4p/*.traj'))
trajs = [t for t in trajs if 'aligned' not in t]
qmmm_adf = ADF(trajs, [0], start=60, pbc=True, world=world, qmidx=[0])
qmmm_adf.run_traj_parallel()
