from jinerate import Templater
import numpy as np

t = Templater(calctype='PES', pestype='roo', h=0.18, mode='lcao')
t.set_defaults(xc='PBE')
t.megadict['gpaw_args']['basis'] = 'tzp'
t.set_paths()
t.megadict['vac'] = 4.
gs = np.linspace(0.755-.4, 0.755+.4, 10)
for g in gs:
    t.megadict['scme_param']['damping_value'] = g
    for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('_PBEtzpSCMEg{0:0.4f}'.format(g), '_SCMEtzpPBEg{0:0.4f}'.format(g) )):
        t.megadict['qmidx'] = qmidx
        t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra=conf)
