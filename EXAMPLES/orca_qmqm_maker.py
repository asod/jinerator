from jinerate import Templater
import numpy as np

 
for PES in ['roo', 'ooh', 'alpha', 'ooxh', 'hoox']:
    for func in ['BLYP', 'PBE']:
        for basis in ['def2-SVP', 'def2-TZVP']:
            t = Templater(calctype='PES', pestype=PES, program='orca',
                          mode='lcao', basis=basis)
            t.orcablocks = '%scf Convergence verytight \n maxiter 300 end \n %pal nprocs 16 end'
            t.set_defaults(xc=func)
            t.tfile = 't_orca_pureqm_pes.py'
            t.megadict['extrapath'] = 'QMQM_' + basis
            t.set_paths()
            t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz')


