from jinerate import Templater
import numpy as np

t = Templater(calctype='PES', pestype='roo', h=0.18)
t.set_defaults(xc='PBE')
t.set_paths()
t.megadict['vac'] = 4.
t.megadict['scme_param'] 
t.megadict['scme_param']['damping_function'] = 'Thole'

t.megadict['extrapath'] = 'thole_tighter'
gs = np.linspace(0.900, 1.3, 10)
for g in gs:
    t.megadict['scme_param']['damping_value'] = g
    for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('_PBE_thole_SCMEg{0:0.4f}'.format(g), '_SCME_thole_PBEg{0:0.4f}'.format(g) )):
        t.megadict['qmidx'] = qmidx
        t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra=conf)
