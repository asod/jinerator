from jinerate import Templater

# GPAW RPBE/SCME grid mode
t = Templater(calctype='Geometry')
t.set_defaults(xc='RPBE')
t.megadict['scme_param']['damping_value'] = 0.7155
t.set_templatefile()
print(t.tfile)
t.make()

