from jinerate import Templater
import numpy as np

for qmidx, conf in zip(([0, 1, 2], [3, 4, 5]), ('QMMM', 'MMQM')):
    t = Templater(calctype='PES', pestype='roo', program='orca', mode='lcao', basis='def2-TZVP')
    t.orcablocks = '%scf Convergence verytight \nmaxiter 300 end\n\n%pal nprocs 16 end\n\n'
    t.set_defaults(xc='BLYP', mmcalc='TIP3P')
    t.tfile = 't_orca_pes.py'
    t.megadict['qmidx'] = qmidx
    t.megadict['extrapath'] = conf + '_TIP3P' 
    t.set_paths()
    t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz')
