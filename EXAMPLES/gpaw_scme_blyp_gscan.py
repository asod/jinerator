from jinerate import Templater
import numpy as np

t = Templater(calctype='PES', pestype='roo')
t.set_defaults(xc='BLYP')
t.set_paths()
t.megadict['vac'] = 7.
for g in np.arange(0.4, 0.6, 0.02):
    t.megadict['scme_param']['damping_value'] = g
    t.megadict['qmidx'] = [0, 1, 2]
    t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra='_BLYPSCME_g{0:03.2f}'.format(g))
    t.megadict['qmidx'] = [3, 4 ,5]
    t.make(t.rootdir+t.xyzdir+'scme_dimer.xyz', extra='_SCMEBLYP_g{0:03.2f}'.format(g))
