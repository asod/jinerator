""" Run from root dir with mpirun -np X python -m EXAMPLES/step_par_adf """

from tools.adf import ADF
import glob
from mpi4py import MPI

world = MPI.COMM_WORLD

trajs = ['/home/asod/Dropbox/HI/SCME/2018_September/liquidTIP4P/tip4p_256MolBox.traj']
mmmm_adf = ADF(trajs, [], start=50, step=10, pbc=True, world=world)
mmmm_adf.run_step_parallel()
