import rmsd
import numpy as np
from itertools import combinations
from ase.io import Trajectory, read
from pathlib import Path

def get_oh(atoms, qmidx):
    pos = atoms[qmidx].get_positions()
    r_ohs = np.zeros((len(pos) // 3, 2))
    for i, o in enumerate(range(0, len(pos), 3)):
        for h in [1, 2]:
            r_ohs[i, h - 1] = np.linalg.norm(pos[o] - pos[o + h])
        
    return r_ohs.ravel()

def rmsd_diff(atoms, qmqm):
    ''' RMSD Distortion of O positions vs single-model results '''
    
    qmmm_pos = np.copy(atoms.positions)[::3]
    qmqm_pos = np.copy(qmqm.positions)[::3]
    
    qmmm_pos -= rmsd.centroid(qmmm_pos)
    qmqm_pos -= rmsd.centroid(qmqm_pos)
    
    U = rmsd.kabsch(qmmm_pos, qmqm_pos)
    qmmm_pos = np.dot(qmmm_pos, U)
    
    final_rmsd = rmsd.rmsd(qmmm_pos, qmqm_pos)
    
    return final_rmsd, qmmm_pos

# Generate all qmidxes into all_qmidx
# You can use all_qmidx to check that all combinations have been
# run... 
mols = [list(range(i, i + 3)) for i in range(0, 6 * 3,3)]
all_qmidx = []
for qmct in range(1, 6):
    c = list(combinations(mols, qmct))
    for pair in c:
        qmmols = sum(pair, [])
        all_qmidx.append(qmmols)


def read_hexamers(qmmmtrajs, mmtrajs=None, qmtrajs=None, convcrit=0.02):
    """ Takes in (sorted) list of qmmm opt trajectories, rmsd's the final
        positions against the MP2 geoms, as well as pure QM and MM opts 
        (if specified) """
    
    data = np.zeros((len(qmmmtrajs), 7))
    data_qm = np.zeros((len(qmmmtrajs), 7))
    data_mm = np.zeros((len(qmmmtrajs), 7))
    ct = 0
    
    # 8 structures
    for struc in range(8):
        here = str(Path(__file__).parent.absolute())  # yes I know ...
        mp2 = read(here + f'/../XYZs/BT{struc:02d}.xyz')
        qmtrjs = [t for t in qmmmtrajs if f'BT{struc:02d}_' in t]
        if mmtrajs:  # there should be only 1 mtrjs though.. 
            mm = [t for t in mmtrajs if f'BT{struc:02d}_' in t]
            assert len(mm) == 1, 'Found more than 1 MM traj!'
            mm = read(mm[0] + '@-1')
        if qmtrajs:
            qm = [t for t in qmtrajs if f'BT{struc:02d}_' in t]
            assert len(qm) == 1, 'Found more than 1 QM traj!'
            qm = read(qm[0] + '@-1')

        for i, tf in enumerate(qmtrjs):
            traj = Trajectory(tf, 'r')
            atoms = traj[-1]
            qmidx = np.where(atoms.get_tags())[0]
            lenqm = len(qmidx) // 3
            mask = np.zeros(len(atoms), bool)
            mask[qmidx] = True
                
            ohs = get_oh(atoms, qmidx)
            max_oh = max(ohs)

            fmax = np.max(np.linalg.norm(atoms.get_forces(), axis=1))
            conv = 1
            if fmax > convcrit:
                print('Unconverged traj: struct: {0:02d}, run: {1:02d},\
                       fmax: {2:2.4f}, max OH: {3:2.4f}'.format(struc, i, fmax, max_oh))
                conv = 0

            diff_mm = np.nan
            diff_mm_qm = np.nan
            diff_mm_mm = np.nan
            diff_qm = np.nan
            diff_qm_qm = np.nan
            diff_qm_mm = np.nan
            
            diff_mp2, _ = rmsd_diff(atoms, mp2)
            diff_mp2_qm, _ = rmsd_diff(atoms[mask], mp2[mask])
            diff_mp2_mm, _ = rmsd_diff(atoms[~mask], mp2[~mask])

            if mmtrajs:
                diff_mm, _ = rmsd_diff(atoms, mm)
                diff_mm_qm, _ = rmsd_diff(atoms[mask], mm[mask])
                diff_mm_mm, _ = rmsd_diff(atoms[~mask], mm[~mask])
            if qmtrajs:
                diff_qm, _ = rmsd_diff(atoms, qm)
                diff_qm_qm, _ = rmsd_diff(atoms[mask], qm[mask])
                diff_qm_mm, _ = rmsd_diff(atoms[~mask], qm[~mask])

            data[ct, :] = np.array([diff_mp2, diff_mm, diff_qm, conv, fmax, lenqm, max_oh])
            data_qm[ct, :] = np.array([diff_mp2_qm, diff_mm_qm, diff_qm_qm, conv, fmax, lenqm, max_oh])
            data_mm[ct, :] = np.array([diff_mp2_mm, diff_mm_mm, diff_qm_mm, conv, fmax, lenqm, max_oh])
            ct += 1

    return data, data_qm, data_mm

def rebin(data, wrt='MM'):
    if wrt == 'MM':
        diff_column = 1
    elif wrt == 'MP2':
        diff_column = 0
    elif wrt == 'QM':
        diff_column = 2
    #rebin to number of QM mols
    converged = data[:, 3] == 1
    all_boxes = [[] for x in range(0, 5)]
    for i in range(1, 6): # loop over all num QMs
        all_boxes[i - 1].extend(data[data[:, 5] == i, diff_column])
    con_data = data[converged]
    con_boxes = [[] for x in range(0, 5)]
    for i in range(1, 6):
        con_boxes[i - 1].extend(con_data[con_data[:, 5] == i, diff_column]) 

    return con_boxes
