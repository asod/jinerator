import numpy as np

''' Functions making geometries for scanning
    of potential (interaction) energy surfaces
    of the water dimer. 

    When used in conjuction with the templater
    it is IMPORTANT that the sequence of the atoms
    in the dimer is as in scme_dimer.xyz, aka:

    O, H, H, O, H, H
    [HDonor, HAcceptor], and 

    HDonor: O, Donating H (Ha), Other H

    A.k.a:
 
             2
             H                
              \           3   H
               O - Ha --- O <
               0   1          H
                              

   The sequence of the last 2 H's does not  matter.

'''              


def get_roo(atoms, movemask):
    # Looks redundant but here to fit the 
    # template pattern for all PES'es
    o2 = movemask[0]
    o1 = abs(o2 - 3)

    pos = atoms.positions
    roo = pos[o2, :] - pos[o1, :]
     
    return roo


def set_roo(atoms, roo, movemask=[0, 1, 2]):
    o2 = movemask[0]
    o1 = abs(o2 - 3)

    pos = atoms.positions
    old_r = get_roo(atoms, movemask)
    v = (old_r / sum(old_r**2)**(.5))
    pos[movemask] += -old_r + roo * v

    atoms.positions = pos



def get_alpha(atoms, o1, o2, acceptor_idx=3):
    # Assuming OHHOHH sequence. 
    aidx = acceptor_idx
    didx = abs(aidx - 3)

    pos = atoms.get_positions()
    vHa = pos[aidx] - pos[aidx + 1]
    vHb = pos[aidx] - pos[aidx + 2]
    V_OO = pos[o1] - pos[o2]
    n = np.cross(vHa, vHb) # normal of HH plane
    alpha = np.arcsin(np.linalg.norm(np.dot(n, V_OO)) / \
            (np.linalg.norm(n) * np.linalg.norm(V_OO)))
    
    # Are Acceptor Hs above or below V_OO?
    # Find out using plane from before
    X = np.dot(atoms[didx].position - pos[aidx + 1], n)
    if X == 0:
        X = 1
    else:
        X /= np.abs(X)
        
    return np.degrees(alpha) * X


def set_alpha(atoms, alpha, movemask=[3, 4, 5], acceptor_idx=3):
    o2 = movemask[0]
    o1 = abs(o2 - 3)
    aidx = acceptor_idx
    alpha0 = get_alpha(atoms, o1, o2)
    
    ### rotate either mol 1 or 2 around V_r = V_x x V_OO
    pos = atoms.get_positions()
    vHa = pos[aidx] - pos[aidx + 1]
    vHb = pos[aidx] - pos[aidx + 2]
    V_x = np.cross(vHa, vHb) # normal of HH plane
    
    V_OO = pos[o1] - pos[o2]
    V_r = np.cross(V_x, V_OO)
    V_r /= np.linalg.norm(V_r)
    
    mol_move = atoms[movemask]
    
    mol_move.rotate(alpha-alpha0, V_r, center=atoms[aidx].position)
    atoms.positions[movemask] = mol_move.get_positions()



def get_ooh(atoms, acceptor_idx=3, h_idx=1):
    # Assuming OHHOHH sequence. 
    aidx = acceptor_idx
    didx = abs(aidx - 3)
    pos = atoms.get_positions()
    V_OO = pos[aidx] - pos[didx]
    V_OH = pos[h_idx] - pos[didx]
    for v in (V_OO, V_OH):
        v /= np.linalg.norm(v)
    
    ooh = np.arctan2(np.linalg.norm(np.cross(V_OH, V_OO)), np.dot(V_OH, V_OO))
    
    # O, O, V_A x V_a plane, to define directionality
    # and to get rotation axis
    vHa = pos[didx] - pos[didx + 1]
    vHb = pos[didx] - pos[didx + 2]
    rot = np.cross(vHa, vHb) # normal of HH plane: Rotation axis
    rot /= np.linalg.norm(rot)
    
    normal = np.cross(rot, V_OO)
    normal /= np.linalg.norm(normal)
    
    # Directionality:
    X = np.dot(atoms[h_idx].position - pos[didx], normal)
    if X == 0:
        X = 1
    else:
        X /= np.abs(X)
                     
    return X * np.degrees(ooh), rot
    

def set_ooh(atoms, ooh, movemask=[0, 1, 2], acceptor_idx=3, h_idx=1):
    aidx = acceptor_idx
    didx = abs(aidx - 3)
    ooh0, rot = get_ooh(atoms, acceptor_idx=acceptor_idx, h_idx=h_idx)
    
    # Rotate direction depends on which mol is moving
    d = 1
    if acceptor_idx in movemask:
        d = -1
    
    rotmol = atoms[movemask]
    rotmol.rotate(-ooh0+ooh, d * rot, atoms.positions[didx])
    atoms.positions[movemask] = rotmol.positions


def get_hoox(atoms, acceptor_idx=3, h_idx=1):
    # Assuming OHHOHH sequence. 
    aidx = acceptor_idx
    didx = abs(aidx - 3)
    
    pos = atoms.get_positions()
    # get x:
    V_OH1 = pos[aidx] - pos[aidx + 1]
    V_OH2 = pos[aidx] - pos[aidx + 2]
    V_HH = V_OH1 - V_OH2
    p_x = pos[aidx + 1] + 0.5 * V_HH
    
    # Get normals of the two dihedral planes HOO and OOX
    V_dOH1 = pos[didx] - pos[h_idx]
    V_OO = pos[aidx] - pos[didx]
    n1 = np.cross(V_dOH1, V_OO)
    n1 /= np.linalg.norm(n1)
    V_OX = p_x - pos[aidx]
    n2 = np.cross(V_OO, V_OX)
    n2 /= np.linalg.norm(n2)
    
    # angle
    hoox = np.arctan2(np.linalg.norm(np.cross(n1, n2)), np.dot(n1, n2))
    
    # Need directionality: Which side of the Donator mol is X on? 
    # Signed distance d = n * (X - x_i), where x_i is any point in the plane
    
    d  = - np.dot(n1, (p_x - pos[didx]))
    
    if not d == 0:
        d /= np.abs(d)  # don't want distance, just sign. 
    
    return d * (np.degrees(hoox) - 180), V_OO / np.linalg.norm(V_OO)


def set_hoox(atoms, hoox, movemask=[3, 4, 5], acceptor_idx=3, h_idx=1):
    # Basically just a rotation around OO axis. 
    hoox0, rot = get_hoox(atoms, acceptor_idx=acceptor_idx, h_idx=h_idx)
    
    # Rotate direction depends on which mol is moving
    d = 1
    if acceptor_idx in movemask:
        d = -1
    
    rotmol = atoms[movemask]
    rotmol.rotate(-hoox0+hoox, d * rot, atoms.positions[movemask[0]])
    atoms.positions[movemask] = rotmol.positions


def get_ooxh(atoms, acceptor_idx=3, h_idx=1):
    # Assuming OHHOHH sequence. 
    aidx = acceptor_idx
    didx = abs(aidx - 3)
    
    pos = atoms.get_positions()
    
    V_OO = pos[aidx] - pos[didx]   
    
    # get X' (here called X:
    V_OH1 = pos[didx + 1] - pos[didx]
    V_OH2 = pos[didx + 2] - pos[didx]
    
    Z = np.cross(V_OH1, V_OH2)
    Z /= np.linalg.norm(Z)
            
    
    X = np.cross(Z, V_OO)
    X /= np.linalg.norm(X)  # Axis X'. Positive is up
                            # This is also rotation axis
    
    # Dihedral angle same as angle between plane normals
    n2 = np.cross(V_OO, X) # V_OO / np.linalg.norm(V_OO)
    n2 /= np.linalg.norm(n2)
    n1 = Z 
    
    ooxh = np.arctan2(np.linalg.norm(np.cross(n1, n2)), np.dot(n1, n2))
    
    # Directionality: Which side of X'OO plane is Ha on?
    # Signed distance d = n * (X - x_i), where x_i is any point in the plane
    
    d  = - np.dot(n2, (pos[h_idx] - pos[aidx]))
    if not d == 0:
        d /= np.abs(d)
    
    return d * np.degrees(ooxh), X
    

def set_ooxh(atoms, ooxh, movemask=[0, 1, 2], acceptor_idx=3, h_idx=1):
    # Assuming OHHOHH sequence. 
    aidx = acceptor_idx
    didx = abs(aidx - 3)
    # Basically just a rotation around OO axis. 
    ooxh0, rot = get_ooxh(atoms, acceptor_idx=acceptor_idx, h_idx=h_idx)
        
    # Rotate direction depends on which mol is moving
    d = 1
    if acceptor_idx in movemask:
        d = -1
    
    rotmol = atoms[movemask]
          
    rotmol.rotate(ooxh-ooxh0, d * rot, atoms.positions[didx])
    atoms.positions[movemask] = rotmol.positions      
