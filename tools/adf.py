#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Function for making angular distribution functions, and counting H-bonds. 
Geometric definition of Water H-bonds from 10.1126/science.1096205.


Uses some of the angles from pesmakers.py

    WIPS: 0. Only works for neat water ! 
          1. Parallelize over trajectories or steps or both
          2. Read other things than .traj



Assuming OHHOHH... Sequence of atoms.


Created on Fri Oct 19 14:39:17 2018

@author: asod
"""
from __future__ import print_function

import numpy as np

from tools.pesmakers import get_hoox, get_ooxh, get_ooh
from ase.io import Trajectory
import sys


m_water = np.array([ 15.9994 ,   1.00794,   1.00794])

def wrap(D, cell, pbc):
    """Wrap distances to nearest neighbor (minimum image convention)."""
    for i, periodic in enumerate(pbc):
        if periodic:
            d = D[:, i]
            L = cell[i]
            d[:] = (d + L / 2) % L - L / 2  # modify D inplace

def qmmic(atoms, qmidx, n):
    ''' Molecule-wise MIC wrap'''
    # Center around c.o.m of QM
    mask = np.zeros(len(atoms), bool)
    mask[qmidx] = True
    qmatoms = atoms[mask]
    mmatoms = atoms[~mask]
    
    qmcenter = qmatoms.get_center_of_mass()
    positions = mmatoms.positions.reshape((-1, n, 3))

    # Distances from the center of the QM box to the first atom of
    # each molecule:
    distances = positions[:, 0] - qmcenter
    wrap(distances, mmatoms.cell.diagonal(), [True, True, True])
    offsets = distances - positions[:, 0]
    positions += offsets[:, np.newaxis] + qmcenter

    pos = np.zeros([len(atoms),3])
    pos[mask] = qmatoms.get_positions()
    pos[~mask] = positions.reshape((-1,3))
    wrappedatoms = atoms.copy()
    wrappedatoms.set_positions(pos)
    return wrappedatoms

def ang(v1, v2):
    cosang = np.dot(v1, v2)
    sinang = np.linalg.norm(np.cross(v1, v2))
    return np.degrees(np.arctan2(sinang, cosang))

def read_adfnpys(npys):
    ''' read and concatenate data from npys containing data sampled using 
        the ADF class. '''
    
    adfs = []
    hbonds = []
    for npy in npys:
        data = np.load(npy, encoding='latin1').item()
        adfs.append(data['adf'])
        hbonds.append(data['hb'])

    # Clean out empty npys to make stacking possible:
    adfs = [x for x in adfs if len(x) > 0]
    hbonds = [x for x in hbonds if len(x) > 0]
    
    adfs = np.vstack(adfs)    
    hbonds = np.hstack(hbonds)
    
    return adfs, hbonds

def plot_adf(data, idx, ax, color='k', binmin=-200, binmax=200, 
                 label='', binsize=1, bonded=True, 
                 extra_filter=None, ls='-'):

    if bonded:
        bonded = np.array(data[:, 7], dtype=bool)
    else:
        bonded = np.ones_like(data[:, 7], dtype=bool)

    id1 = idx
    if extra_filter is not None:
        data = data[extra_filter * bonded][:, id1]
    else:
        data = data[bonded][:, id1]
    
    
    y, binEdges, = np.histogram(data, bins=np.arange(binmin, binmax, binsize), 
                                density=True)
    
    x = 0.5 * (binEdges[1:] + binEdges[:-1])
    
    ax.plot(x, y, color=color, linestyle=ls, label=label)
   
    return ax


class ADF:
    """ Make angular distribution functions of water.
    
    NB!: This version has ONLY been tested for neat water. 

    
    trajectory_list: List of paths to trajectories to sample
    
    sample_idx: Indexes of molecules to sample from. Eg:
                [0, 1, 6] samples from atom 0, 3 and 18, assuming OHHOHH...- 
                sequence. 
                Leave empty if you have a box of neat water, 
                and are not interested in any single mol(s), but the whole box.
                
    sample_within: Sphere to sample within. Most often first solvation shell.
    
    start, stop, step: ... of trajectories to sample

    maxmols: Max number of molecules within first solvation shell 
             Somewhat unstable implemenation currently... 
             Choose too big, and you get into memory errors.
             Too small, and the sampling will crash.
             HOWEVER, automating it more could lead to bad sampling, so 
             maybe its better to crash and find the error.

    optimum_angle: Distortion from linearity of hydrogen bonding. Default 
                   value is that of water. 
                 
    pbc: Use peridic boundary conditions when sampling

    tetra_threshold: At what r the coordination number is 4.00

    world: use the mpi4py world if you want to parallelize over trajectories.                    
                
    """     
    def __init__(self, trajectory_list, sample_idx=[], sample_within=3.35,
                 start=0, stop=-1, step=1, maxmols=20, world=None, 
                 optimum_angle=0., pbc=True, qmidx=[], tetra_threshold=3.2):
        self.oa = optimum_angle # 4.8140864259098928
        self.trajlist = trajectory_list
        self.start = start
        self.stop = stop
        self.step = step
        self.maxmols = maxmols
        self.pbc = pbc 
        self.sidx = sample_idx 
        self.sw = sample_within
        self.world = world
        self.qmidx = qmidx  # molecule-wise QM index. For filtering on 
                            # QM/MM vs MM/QM configurations 

        self.tetra_threshold = tetra_threshold
        # Remember to update this with what you put in out in sample()
        self.adf_format = ['alpha (deg.)', 'theta1 (deg.)', 'theta2 (deg.)', 
                          'beta (deg.)', 'omega(deg.)', 'hoox (deg.)',
                          'ooxh (deg.)', 'bonded (bool)', 
                          'Omega defined (bool)', 'd_is_qm (bool)',
                          'Od', 'H1d', 'H2d',
                          'Oa', 'H1a', 'H2a']
            


    def run_traj_parallel(self):
        """ Superduper simple parallelisation over individual trajectories.
            dependencies: MPI4PY.
            
            See: ../EXAMPLES/par_adf.py
            """
        par_trajs = np.array_split(self.trajlist, self.world.size)
        for c, chunk in enumerate(par_trajs):
            if self.world.rank == c:
                self.make_adfs(chunk)

    def run_step_parallel(self):
        """ Simple parallelisation over steps in a single trajectory. 
            Dependencies: MPI4PY. """
        step = self.step
        for traj in self.trajlist:
            stop = self.stop
            start = self.start
            try:
                print(traj)
                t = Trajectory(traj, 'r')
            except:
                print('*** Trajectory: {0:s} could not be read!'.format(traj))
                continue
            if stop < 0:
                stop = len(t) + stop

            step_range = range(start, stop, step)
            chunks = np.array_split(step_range, self.world.size)
            for c, chunk in enumerate(chunks):
                if self.world.rank == c:
                    adfs, all_hbonds, all_ooo = self.sample(t, chunk[0], chunk[-1], 
                                                   self.step)
                    npyf = traj.split('.traj')[0] +\
                           '_{0:02d}'.format(c) + '.npy'

                    outdict = {'adf': adfs,   # XXX WIP: Should be pre-init'ed a single place!
                               'hb':all_hbonds,
                               'ooo':all_ooo,
                               'trajectory':traj,
                               'start':self.start,
                               'stop':self.stop,
                               'step':self.step,
                               'adf_format': self.adf_format}
                    
                    np.save(npyf, outdict)


    def make_adfs(self, trajlist=None):
        """ Distribute workload over cores (WIP), sample and save """
        
        if trajlist is None:
            trajlist = self.trajlist
            
        for traj in trajlist:
            print('*** Trajectory: {0:s}'.format(traj))
            try:
                t = Trajectory(traj, 'r')
            except:
                print('*** Trajectory: {0:s} could not be read!'.format(traj))
                continue
            
            adfs, all_hbonds, all_ooo = self.sample(t, self.start, self.stop, self.step)
            npyf = traj.replace('.traj', '.npy')
            
            outdict = {'adf': adfs, 
                       'hb':all_hbonds,
                       'ooo':all_ooo,
                       'trajectory':traj,
                       'start':self.start,
                       'stop':self.stop,
                       'step':self.step,
                       'adf_format': self.adf_format}
            
            np.save(npyf, outdict)
        
        
    def sample(self, traj, start, stop, step):
        # If mols is None, loop over all water mols. 
        # else, the sampling will go over all waters.
        
       
        sidx = self.sidx
        
        mols = (len(traj[0])) // 3  # XXX WIP: Change to work for mixed systems
        
        loop_range = list(range(mols - 1))
        if len(sidx) != 0:
            loop_range = sidx

        if stop < 0:
            stop = len(traj) + stop

        step_range = range(start, stop, step)
        numsteps = len(step_range)
        
        if start > len(traj):
            print("Trajectory too short")
            return np.array([]), np.array([]), np.array([])
        
        # init output arrays
        # Init'ing arrays, you easily run into mem errors here. 
        # You need more molecules than you can possibly think will be
        # in the first solvation shell, but if you just choose ALL mols,
        # youll run out of memory.. 
        adfs = np.empty((self.maxmols * len(loop_range) * numsteps,
                         len(self.adf_format)))
        
        all_hbonds = np.zeros((len(loop_range) * numsteps))

        all_ooo = np.empty((self.maxmols * len(loop_range) * numsteps))

        ct = 0
        mol_ct = 0
        tri_ct = 0
        # loop over steps in traj
        for s in step_range:
            atoms = traj[s]
            atoms.constraints = []
            
            sum_within = 0
            
            # loop over molecules 
            for i in loop_range:
                mol1_is_qm = False
                if i in self.qmidx:
                    mol1_is_qm = True
                mol1idx = range(i * 3, i * 3 + 3)
                if self.pbc:
                    atoms = qmmic(atoms, mol1idx, len(mol1idx))
                    
                
                mol1 = atoms[mol1idx]
                
                pos = atoms.get_positions().reshape((mols, 3, 3))
                
                allcom = np.dot(m_water, pos) / m_water.sum()
                mol1com = np.dot(m_water, mol1.positions) / m_water.sum()
                
                moldists = np.apply_along_axis(np.linalg.norm, 1, 
                                               allcom - mol1com)
                
                #  only look at closest waters 
                within = moldists <= self.sw
                w_idx, = np.where(within)
                sum_within += sum(within)
                
                # Get 2 nearest neighbours for Tetrahedrality stuff
                d_trip = self.tetra_threshold # Where OO cordination number reaches 4.0. YOU HAVE TO FIND THAT
                # XXX WIP: NEED SOME QM MOL IDENTIFICATION HERE
                for k in w_idx:
                    for l in w_idx:
                        if (k == l) or (k == i) or (l == i):
                            continue
                        m2 = 3 * k
                        m3 = 3 * l

                        v1 = atoms[m2].position - atoms[mol1idx[0]].position
                        v2 = atoms[m3].position - atoms[mol1idx[0]].position
                        
                        d1 = np.linalg.norm(v1)
                        d2 = np.linalg.norm(v2)

                        if (d1 < d_trip ) and (d2 < d_trip):
                            all_ooo[tri_ct] = ang(v1, v2)
                            tri_ct += 1



                # init hbonds for this shell
                hbonds = 0

                # loop
                for j in w_idx:
                    if j == i:  # skip distance to itself
                        continue
                    mol2_is_qm = False

                    if j in self.qmidx:  #yes, its j to ask for, not mol2idx
                                         #since qmidx is molwise in this script
                        mol2_is_qm = True
                    mol2idx = list(range(j * 3, j * 3 + 3))

                    mol2 = atoms[mol2idx]  # OHHOHH, ...
                    # Find donor and acceptor
                    (don, acc, hd, 
                     d_is_qm) = self.water_dimer_identify(mol1, mol2,
                                                          mol1_is_qm,
                                                          mol2_is_qm)
                    # Get angles
                    alpha = self.get_alpha(don, acc, hd)
                    theta = [(don + acc).get_angle(hd, 3, x) for x in [4, 5]] 
                    beta =  np.abs(get_ooh(don + acc, 3, hd)[0])
                    
                    omega = self.get_omega2(don, acc, hd)
                    
                    hoox = get_hoox(don + acc, 3, hd)[0]
                    ooxh = get_ooxh(don + acc, 3, hd)[0]
                    
                    # Define if bonded, from the beta-based bonding crit.:
                    roo = self.get_roo(don, acc)
                    bonded = roo < self.sw - 0.00044 * (beta - self.oa)**2
                    om_def = self.is_omega_defined(don, acc, hd)
                    
                    # Count hydrogen bonds
                    if bonded:
                        hbonds += 1

            
                    out = ([alpha] + theta + [beta] + [omega] + [hoox] +
                           [ooxh] + [bonded] + [om_def] + [d_is_qm] + 
                           list(mol1idx) + list(mol2idx))
                    
                    adfs[ct, :] = out
                    ct += 1
                
                all_hbonds[mol_ct] = hbonds
                mol_ct += 1
                outstr = 'Step: '+str(s).zfill(4) + ' of '+str(stop) +\
                         ', mol {0:d} of {1:d}, Hbonds: {2:d} '.format(i, 
                                                                   mols,
                                                                   hbonds)
                print(outstr, end='\r')
                sys.stdout.flush()
                #self.parprint(outstr)  XXX WIP

        adfs = adfs[:ct, :]
        all_ooo = all_ooo[:tri_ct]
        
        return adfs, all_hbonds, all_ooo
                    

    def water_dimer_identify(self, mol1, mol2, mol1_is_qm, mol2_is_qm):
        atoms = mol1 + mol2
        h_dists = [np.linalg.norm(atoms[y - x].position - atoms[x].position) 
                   for x in [0, 3] for y in [4, 5]]
        short_h = np.argmin(h_dists)
        hdonor_idx = [4, 5, 1, 2][short_h]

        donor_is_qm = False

        if hdonor_idx > 3:  # mol2 was donor
            od_idx = 3
            hd_new = hdonor_idx - 3
            if mol2_is_qm:
                donor_is_qm = True
        else:  # mol1 was donor
            od_idx = 0
            hd_new = hdonor_idx
            if mol1_is_qm:
                donor_is_qm = True
        
        mask = np.zeros(6, bool)
        mask[od_idx:od_idx + 3] = True
        donor = atoms[mask]
        acceptor = atoms[~mask]
        
        # return donor, acceptor, and new H donor idx
        return donor, acceptor, hd_new, donor_is_qm 


    def is_omega_defined(self, don, acc, hd):
        ''' Omega is only well-defined if there is exactly one H on either
            side of the (OdOa, OdHd)-plane...    '''
        
        atoms = don + acc
        pos = atoms.get_positions()
        
        V_OdOa = pos[3] - pos[0]
        V_OdHd = pos[hd] - pos[0]
        
        # Plane normal
        #X = np.cross(V_OdOa, np.cross(V_OdOa, V_OdHd))
        X = np.cross(V_OdOa, V_OdHd)
        X /= np.linalg.norm(X)
        
        # Signed distance of point p to plane with unit normal X
        # d = X * (p - p_i), where p_i is any given point in the plane
        p_i = pos[0]
        d1 = np.dot(X, (pos[4] - p_i))
        d2 = np.dot(X, (pos[5] - p_i))
        
        #return d1 * d2 > 0
        return d1 * d2 < 0
        
     
    def get_roo(self, mol1, mol2):
        return np.linalg.norm(mol1[0].position - mol2[0].position)
 
    
    def get_alpha(self, mol1, mol2, h):
        pos = (mol1 + mol2).get_positions()
        v1 = pos[0] - pos[h]
        v1 /= np.linalg.norm(v1)
        v2 = pos[3] - pos[h]
        v2 /= np.linalg.norm(v2)
    
        return ang(v1, v2)

    
    def get_beta(self, mol1, mol2, h):
        pos = (mol1+mol2).get_positions()
        v1 = pos[h] - pos[0]
        v1 /= np.linalg.norm(v1)
        v2 = pos[3] - pos[0]
        v2 /= np.linalg.norm(v2)
        
        return ang(v1, v2)
    
    def get_omega2(self, don, acc, hd, debug_writer=None):
        ''' Wagging angle, previously called alpha, 
            but this sampling has a more robust sign definition 
            needed for liquid. '''
            
        
        pos = (don + acc).get_positions()
        vHa1 = pos[3] - pos[4]
        vHa2 = pos[3] - pos[5]
        vOaOd = pos[0] - pos[3]
        pX = pos[4] + 0.5 * (pos[5] - pos[4])
        vOaX = pX - pos[3]
        nOaX = vOaX / np.linalg.norm(vOaX)
        
        vOdOa = pos[3] - pos[0]
        nOdOa = vOdOa / np.linalg.norm(vOdOa)
        VOdHd = pos[hd] - pos[0]
        nOdHd = VOdHd / np.linalg.norm(VOdHd)
        out = np.cross(nOdOa, nOdHd)
        out /= np.linalg.norm(out)
        
        up = np.cross(out, nOdOa)
        
        
        # normal vector of plane where X lies
        n = np.cross(vHa1, vHa2)
        n /= np.linalg.norm(n)
        
        omega = np.arcsin(np.linalg.norm(np.dot(n, vOaOd)) / \
                (np.linalg.norm(n) * np.linalg.norm(vOaOd)))
        
        # Get orientation based on whether Hd2 is pointing towards or 
        # away from plane
        if hd == 1:
            hd2 = 2
        elif hd == 2:
            hd2 = 1
        
        vOdHd2 = pos[hd2] - pos[0]
        nOdHd2 = vOdHd2 / np.linalg.norm(vOdHd2)
    
        up *= np.dot(nOdHd2, up) / np.abs(np.dot(nOdHd2, up))
      
        #direc = np.dot(nOdHd2, nOaX)
        direc = np.dot(up, nOaX)
        direc /= np.abs(direc)
        
        #print(direc, np.degrees(omega)*direc)
        
        if debug_writer is not None:
            atoms = don + acc
            v = 0 * atoms.get_positions()
            v[0] = up * direc
            v[3] = nOaX #nOdHd2
            atoms.set_velocities(v)
            debug_writer.write(atoms)
        
        return np.degrees(omega) * direc
    
    def parprint(self, outstr):
        ''' WIP to the point of not working '''
        
        if self.world is None:
            print(outstr, end='\r')
        else:
            rank = self.world.rank
            alloutstr = self.world.gather(outstr, root=0)
           
            if rank == 0:
                ma = []
                for i, s in enumerate(alloutstr):
                    s = 'CPU  {0:02d}: '.format(i) + s
                    ma.append(s)  
                print('\n'.join(ma)+'\r', end='\n')
                sys.stdout.flush()
        
        sys.stdout.flush()

