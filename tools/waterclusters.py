import numpy as np
import os
from ase.units import Bohr, Hartree
import itertools as itools



def make_full_idx(nummols):
    ''' create full list of possible qm indices '''
    qmmmrange = range(nummols)
    qmmrange = qmmmrange[1:] # QM number of mols (can be 1, 2, 3...)
    mols = [list(range(i, i + 3)) for i in range(0, nummols * 3, 3)]  # OHHOHH... idx
    full_idx = []
    for qmct in qmmrange:  # so this excludes full QM
        molcombos = list(itools.combinations(mols, qmct))
        for pair in molcombos:
            qmmols = sum(pair, [])
            full_idx.append(','.join(str(a) for a in sum(pair, [])))

    return full_idx


def missing_from_npy(npy, nummols):
    if isinstance(npy, str):
        data = np.load(npy).item()
    elif isinstance(npy, dict):
        data = npy
    else:
        raise IOError('Data type not understood')
    full_idx = make_full_idx(nummols)
    qmmm_calcs = [k for k in list(data.keys()) if isinstance(k, int)]
    already_run = []
    for q in qmmm_calcs:
        d = data[q]
        already_run.append(','.join(str(a) for a in (d['qm_idx'])))
    missing = []
    for f in full_idx:
        if f not in already_run:
            missing.append(f)
    
    return missing

def parse_clusterdat(datfile):
    if os.path.isfile(datfile):
        import numpy as np
        with open(datfile,'r') as f:
            lines = f.readlines()
        numdatlines = len(lines) - 1
        header = lines[0].split()
        header = [header[0]]+header[2:]  # get rid of idx in header
        rawdata = lines[1:]  # get read of header in raw data
        data = np.zeros([len(rawdata), len(header)])
        idx = []
        for i,d in enumerate(rawdata):
            dd = d.split()
            if len(dd) == 0:
                continue
            data[i] = np.array([float(x) for x in dd if not any(s in x for s in (',','nan'))])
            idx.append([x for x in dd if any(s in x for s in (',','nan'))][0])
    else:
        data = None
        header = None
        idx = []
    return data, header, idx


def get_nummols(datfile):
    if 'BT' in datfile.split('/')[-1]:
        nummols = 6
    elif 'water' in datfile.split('/')[-1]:
        nummols = int(datfile.split('water')[-1][0])
        if nummols == 1:
            nummols = 10  
    else:
        raise ValueError('Cannot parse # of mols from file name: '+datfile.split('/')[-1])
    return nummols

def check_complete(datfile, idx, nummols=None):
    import itertools as itools
    if os.path.isfile(datfile):
        nummols = get_nummols(datfile)

    full_idx = make_full_idx(nummols)
    missing = []
    for x in full_idx:
        if x not in idx:
            missing.append(list(map(int, x.split(','))))
    # check missing pure QM and pure MM
    qm_missing = []
    qmidx = ','.join(str(q) for q in range(nummols*3))
    if qmidx not in idx:
        qm_missing.append(range(nummols*3))
    mm_missing = []
    if np.nan not in idx:
        mm_missing.append([np.nan])

    return missing, qm_missing, mm_missing

def file_len(fname):
    # if file doesnt exist or empty, it returns 0
    i = -1
    try:
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
    except:
        i = -1
    return i + 1

def writeout(fname, numqm, idx, e, first, longest, outnames=None):
    if outnames is None:
        outnames = ['NumQM','QMid', 'Emonos', 'Ecluster', 'Eint', 'E_octhex']
    totalenespec = ''
    numenes = len(e)
    for n in range(numenes):
        totalenespec+='{'+str(n+2)+':16.5f}'
    idxspec = '{1:<'+str(longest)+'}'
    spec = '{0:<16.5f}'+idxspec+totalenespec
    outidx = ','.join(str(a) for a in idx)
    outline = spec.format(numqm,outidx, *e)

    if first:
        headqm = '{0:<16}{1:<'+str(longest+9)+'}'
        headenes = ''
        for n in range(numenes):
            headenes += '{'+str(n+2)+':<16}'
        headoutspec = headqm+headenes
        headout = headoutspec.format(*outnames)
        with open(fname, 'w') as ofile:
            ofile.write(headout)
            ofile.write('\n')
            print(headout)

    with open(fname, 'a') as out:
        out.write(outline)
        out.write('\n')
        print(outline)


def init_datafile(path, name, outnames=None, parameters=None, kwargs=None, **more):

    with open(path + name + '.dat', 'w') as ofile:
        if parameters:
            ofile.write('SCMEEmbedding Parameters:')
            ofile.write('\n')
            for key, val in parameters.iteritems():
                ofile.write('{0:<35}{1:<15}'.format(key, val))
                ofile.write('\n')
            ofile.write('\n')
        if kwargs:
            ofile.write('GPAW Parameters: ')
            ofile.write('\n')
            for key, val in kwargs.iteritems():
                ofile.write('{0:<35}{1:<15}'.format(key, val))
                ofile.write('\n')
            ofile.write('\n')
        if more is not None:
            for key,val in more.iteritems():
                ofile.write('{0:<35}{1:<15}'.format(key, val))
                ofile.write('\n')
            ofile.write('\n')

        if outnames:
            ofile.write('##################### DATA #####################')
            ofile.write('\n')

            ofile.write(''.join(format(x, 's').ljust(12) for x in outnames))
            ofile.write('\n')

def datafile_write(fname, allout):
    with open(fname, 'a') as out:
        out.write(''.join(format(i, '2.5f').ljust(12) for i in allout))
        out.write('\n')


def get_datafile(fname):
    """ currently just skips the extra information you can read for yourself
        in the data file. """
    f = open(fname)
    lines = f.readlines()
    f.close()
    out = {}
    done = False
    first_dataline = None
    for l, line in enumerate(lines):
        if '####### DATA ######' in line:
            first_dataline = l+2
            data_header = lines[l+1].split()
    if not first_dataline:
        raise ValueError('No data found')

    data = np.genfromtxt(fname, skip_header=first_dataline)

    return data, data_header

# for simpler datafiles
def get_data(file):
    data = np.genfromtxt(file,skip_header=1)
    # Get header
    with open(file,'r') as f:
        line = f.readline()
    params = line.split()
    return data,params



# MM Coloumb interaction energy, Crystal adds this, we must then subtract it.
# simply E_12 = q1q2 / d_12 and then for all pairs

def mm_coulomb(atoms):
    E = []
    pos = atoms.calc.add_virtual_sites(atoms.positions)
    pos /= Bohr
    chg = atoms.calc.get_virtual_charges(atoms)
    for n in range(len(pos) - 1):
        for m in range(n+1, len(pos)):
            d = np.sqrt(np.sum((pos[m] - pos[n])**2))
            E.append(chg[m] * chg[n] / d)

    return np.sum(E) * Hartree


