import numpy as np
from matplotlib import pyplot as plt

''' Collection of functions to help make the clusterplots, should 
    probably be refactored into a class of sorts, too many arguments
    and in/out... 
    
    Check the plot_clusters.py example in EXAMPLES. ''' 

ncs = plt.rcParams['axes.prop_cycle'].by_key()['color']


def makenice(bp, vp):
    for box in bp['boxes']:
        box.set(edgecolor='k', linewidth=.8, facecolor='w')
    for whisker in bp['whiskers']:
        whisker.set(linestyle='--', dashes=(5,1.5), color='k', linewidth=0.8)
    for flier in bp['fliers']:
        flier.set(marker='o', color='#e7298a', alpha=0.5, markersize=1)
    for pc in vp['bodies']:
        pc.set_facecolor('white')
        pc.set_edgecolor('black')
        pc.set_alpha(.75)  
    

def get_minmax_patches(func, nummols):
    diffs = [x[0] for x in func.pure_ddes if x[1] == nummols]
    return np.min(diffs), np.max(diffs)
    
    
def make_clusterplot(ax, func):    
    boxes = func.rebin_pr_numqm(func.out['alldeints'])
    allpos = []
    ct = 0
    for b in boxes:
        pos1 = np.arange(len(b)) + ct
        allpos.extend(pos1)
        ct += len(b) + 1
    
    ######### Patches - violins have no zorder :(
    minmax = [get_minmax_patches(func, x) for x in [len(x) + 1 for x in boxes]]
    positions = allpos
    xbars = sum([[x -.5, x + .5] for x in list(range(1, positions[-1] + 2)) if x not in positions], [])
    xbars = [- 0.5] + xbars[0:-1]
    xbars = np.reshape(np.asarray(xbars), (len(xbars) // 2, 2))
    patches = []
    for i,xb in enumerate(xbars):
        xval = [xb[0], xb[1]]
        col1 = ncs[0]
        col2 = ncs[3]
        if minmax[i][0] < 0:
            b = ax.fill_between(xval, 0, minmax[i][0], facecolor=col1, alpha=0.35, zorder=1, label='Max single-model difference');
            patches.append(b)
        if minmax[i][1] > 0:            
            b = ax.fill_between(xval, 0, minmax[i][1], facecolor=col2, alpha=0.35, zorder=1, label='Max single-model difference');
            patches.append(b)
        
    ct = 0
    xtics = []
    for b in boxes:
        pos1 = np.arange(len(b)) + ct
        xtics.extend(np.arange(1, len(b) + 1))

        bp = ax.boxplot(b, positions=pos1, meanline=False, patch_artist=True, zorder=10, widths=.3)
        vp = ax.violinplot(b, positions=pos1, widths=0.75, showextrema=False, points=100)

        makenice(bp, vp)
        ct += len(b) + 1
        
    maxmax = max(sum(func.out['alldeints'], []))
    minmin = min(sum(func.out['alldeints'], []))
    tensig = 10 * np.std(sum(func.out['alldeints'], []))
    onesig = 1 * np.std(sum(func.out['alldeints'], []))
    
    ax.set_xticks(allpos)
    ax.set_xticklabels(xtics, size=14)
    ax.set_xlim([allpos[0] - 1, allpos[-1] + 1])
    ax.set_ylim([minmin - 0.75 * onesig, maxmax + 0.75 * onesig])
    #ax.set_ylim([-35, 80])
    
    ax.yaxis.grid(b=True)
    return xbars, patches


def totalmols(ax, xbars, y=None):
    totmols = np.arange(3, 11)
    if not y:
        y=ax.get_ylim()[1] + 3
    for i, xb in enumerate(xbars):
        x = np.mean(xb)
        t = ax.text(x, y, str(totmols[i]), horizontalalignment='center')
