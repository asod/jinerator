import numpy as np
from ase.constraints import FixBondLengths
from ase.io import Trajectory, read
from ase.calculators.orca import ORCA
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions
from ase.calculators.tip3p import TIP3P, epsilon0 as eps3, sigma0 as sig3
from ase.calculators.tip4p import TIP4P, epsilon0 as eps4, sigma0 as sig4
from ase.optimize.fire import FIRE

import sys
sys.path.append('{{ rootdir|string() }}')

path = '{{ rundir|string() }}'


qmidx = [{{ qmidx }}]
the_file = '{{ xyzfile }}'
kwargs = {{ orca_args }}
xc = '{{ xc }}'
basis = '{{ basis }}'

tag = the_file.split('/')[-1].split('.xyz')[0]
tag += '{{ extra }}'


atoms = read(the_file)

mask = np.zeros(len(atoms), bool)
mask[qmidx] = True
mmidx = np.where(~mask)[0]

# fix all bonds
c1 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                     for i in range(len(atoms) // 3)
                     for j in [0, 1, 2]])

atoms.constraints = c1
# Save what is QM using the tags
tags = np.zeros(len(atoms))
tags[mask] = 1
atoms.set_tags(tags)

emb = {{ embedding|string() }}
interaction = {{ interaction|string() }}


atoms.calc = EIQMMM(selection=qmidx,
                    qmcalc=ORCA(label=path + '/outputfiles/' + tag + '.out',
                                orcasimpleinput=kwargs['orcasimpleinput'],
                                orcablocks=kwargs['orcablocks']),
                    mmcalc={{ mmcalc|string() }}{{ mmcalc_args|string()}}, 
                    interaction=interaction,
                    embedding=emb,
                    output=path + '/outputfiles/' + tag + '_QMMM.log')

opt = FIRE(atoms, logfile=path + '/' + tag + '_opt.log')
traj = Trajectory(path + '/' + tag + '.traj', 'w', atoms)
opt.attach(traj.write, interval=1)
opt.run(fmax={{ fmax }}, steps={{ geom_steps }})

