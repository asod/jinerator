import itertools as itools
from ase.units import Hartree, Bohr
from gpaw.eigensolvers import CG, RMMDIIS
import numpy as np
from gpaw import GPAW, FermiDirac, Mixer
from ase.parallel import rank, broadcast
from ase.io.trajectory import TrajectoryWriter
from ase.io import read
from ase import Atoms
from gpaw.cluster import Cluster
import time
from os.path import isfile
# for now just import ALL THE THINGS
try:
    from ase.calculators.qmmm import SCMEEmbedding
    from ase.calculators.scme.calc_qmscme import Calc_QMSCME  # interaction
    from ase.calculators.scme.new_scme_interface import SCME_Interface
except:
    pass
from ase.calculators.tip3p import TIP3P, epsilon0 as eps3, sigma0 as sig3
from ase.calculators.tip4p import TIP4P, epsilon0 as eps4, sigma0 as sig4
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions

# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
#
from waterclusters import (parse_clusterdat, get_nummols, check_complete, 
                           writeout, make_full_idx)

path = '{{ rundir|string() }}'

# define params
vac = {{ vac }}
kwargs = {{ gpaw_args }}
for eval_things in ['occupations', 'mixer', 'eigensolver']:
    if eval_things in kwargs:
        kwargs[eval_things] = eval(kwargs[eval_things])
mode = 'grid'

if 'mode' in kwargs:  #check if lcao
    mode = kwargs['mode']

parameters = {{ scme_param }}
g = parameters['damping_value']

the_file = "{{ xyzfile }}"
atoms = read(the_file)
nummols = len(atoms) // 3
tag = the_file.split('/')[-1].split('.xyz')[0]

longest = len(','.join(str(a) for a in range(len(atoms))))

atoms.center(vacuum=vac)
# and then fix the cell to get exactly the same grid spacing
cluster = Cluster(Atoms(atoms.get_chemical_symbols(),
                        atoms.get_positions()))
cluster.minimal_box(vac, kwargs['h'])
qm_cell = cluster.get_cell()
atoms.set_positions(cluster.get_positions())

atoms.set_cell(qm_cell)

first = False
outnames = ['NumQM', 'QMid', 'Emonos', 'Ecluster', 'Eint', 'E_octhex']
mols = [range(i, i+3) for i in range(0, nummols * 3, 3)]  # OHHOHHOHH, ...
name = tag+'_grid_'

### CHECK IF THIS HAS BEEN RUN BEFORE
### WIP: Refactor into new dict.. 
data, header, idx = parse_clusterdat(path+name+'.dat')
if not idx:
    nummols = get_nummols(path+'xyz_files/'+the_file)
    missing, qm_missing, mm_missing = check_complete(path+name+'.dat', idx, nummols)
else:
    missing, qm_missing, mm_missing = check_complete(path+name+'.dat', idx)

if (not missing) and (not mm_missing) and (not qm_missing):
    raise ValueError('All data points already collected in: '+path+name+'.dat')

if data is None:  # First run
    first = True
    data = np.zeros((1, 1)) * np.nan  # to get pure qm and pure mm to work


# INIT OUTPUT DICT. WIP: "missing" should be in here too.
# and check if already exists!
outdictname = path+name+'.npy'
fastqmdictname = path+name+'qm_monos.npy'
if not first:
    if rank == 0:
        OD = np.load(outdictname).item()
        if isfile(fastqmdictname):
            QM = np.load(fastqmdictname).item()
        else:
            QM = {}
else:
    if rank == 0:
        OD = {}
        OD['JOB_INFO'] = {}
        OD['JOB_INFO']['GPAW_ARGS'] = kwargs
        OD['JOB_INFO']['SCME_ARGS'] = parameters
        OD['JOB_INFO']['vac'] = vac
        OD['JOB_INFO']['PATH'] = path
        OD['JOB_INFO']['NAME'] = name
        OD['JOB_INFO']['atoms'] = atoms

        QM = {}

if rank == 0:
    np.save(outdictname, OD)
    np.save(fastqmdictname, QM)


### QMMM
step = 0
n = 3
full_idx = make_full_idx(nummols)
for qmmols in missing:
    qmidx = qmmols
    ### Update outdict
    if rank == 0:
        OD = np.load(outdictname).item()
    run_no = full_idx.index(','.join(str(x) for x in qmmols))
    if rank == 0:
        OD[run_no] = {}

    qmct = len(qmmols) / 3
    pair = [qmmols[i:i + n] for i in range(0, len(qmmols), n)]
    # make QM subsystem out of ALL QM mols
    mask = np.zeros(len(atoms), bool)
    mask[qmmols] = True
    mmatoms = atoms[~mask]
    e_mono = 0
    # get individual monomer energies
    if rank == 0:
        OD[run_no]['qmmm_e_monos'] = {}
    for m, monomol in enumerate(pair):
        cp_mask = np.zeros(len(atoms), bool)
        cp_mask[monomol] = True
        qmatoms = atoms[cp_mask]
        
        if mode.lower() == 'lcao':
            setups = np.array(['paw' if s else 'ghost' for s in cp_mask])
            setups = dict([(j, s) for j, s in enumerate(setups[mask])])
            kwargs['setups'] = setups
            qmatoms = atoms[mask]  # entire QM atoms, but with ghost setups for CP

        if rank == 0:
            QM = np.load(fastqmdictname).item()
        else:
            QM = None
        QM = broadcast(QM)

        # for LCAO the same monomol has different energies depending on ghost basis, so 
        # disable the fast loading.
        if (str(monomol) in QM) and (kwargs['mode'].lower() != 'lcao'):
            e_this_mono = QM[str(monomol)]
            if rank == 0:
                print(str(monomol)+' single QM energy already in dict, just loading')
        else:
            if rank == 0:
                print(str(monomol)+' single QM energy not in dict, calculating')
            outtxt= path + 'outputfiles/' + name+'numQM' + str(qmct) +\
                    'pair_FM_mono_' + str(m).zfill(2)
            qmatoms.set_cell(qm_cell)
            qmatoms.calc = GPAW(txt=outtxt+'.out', **kwargs)
            e_this_mono = qmatoms.get_potential_energy()
            QM[str(monomol)] = e_this_mono

        if rank == 0:
            np.save(fastqmdictname, QM)

        e_mono += e_this_mono
        if rank == 0:
            OD[run_no]['qmmm_e_monos'][m] = e_this_mono
        qmatoms.calc = None

    interaction = {{ interaction|string() }}
    setups = {None: 'paw'}  ## Remove potential ghost atoms from setups again
    kwargs['setups'] = setups  

    outtxt = path + 'outputfiles/' + name + 'numQM' +\
             str(qmct) + '_qmmm_qm_FM'
    if rank == 0:
        print('calculating QMMM for QM conf: '+str(qmmols))

    eiqmmm_kwargs = {} 
    if hasattr(interaction, 'w_idx'):  # SCME 
        eiqmmm_kwargs['qm_cell'] = qm_cell
        atoms.set_cell([200, 200, 200])

    atoms.calc = EIQMMM(selection=qmmols,
                        qmcalc=GPAW(txt=outtxt+'.out',
                                    **kwargs),
                        mmcalc = {{ mmcalc|string() }},
                        interaction=interaction,
                        embedding = {{ embedding|string() }},
                        output=outtxt+'QMMM.log',
                        **eiqmmm_kwargs)
    tstart = time.time()
    ehex = atoms.get_potential_energy()
    tend = time.time()
    t_qmmm = tend - tstart 
    eint = ehex - e_mono
    # Eoh doesnt make sense anymore 
    Eoh = 0.0 #atoms.calc.qmcalc.hamiltonian.vext.Eoh * Hartree  
    ### SAVE STUFF TO OUTPUT DICT -- seems like it only wants to live on 0
    positions = atoms.get_positions()
    qm_positions = atoms[qmmols].get_positions()
    mm_positions = mmatoms.get_positions()
    qm_dipole = atoms.calc.qmcalc.get_dipole_moment()

    forces = atoms.get_forces()
    ## SCME SPECIFIC
    if hasattr(atoms.calc.mmcalc, 'dpoles'):
        mm_dipoles = atoms.calc.mmcalc.dpoles
        mm_dipolesQM = atoms.calc.mmcalc.dpolesQM
        
        mm_qpoles = atoms.calc.mmcalc.qpoles
        mm_qpolesQM = atoms.calc.mmcalc.qpolesQM
        
        scme_forces = atoms.calc.mmcalc.forces
        scme_forcesCM = atoms.calc.mmcalc.fCM
        f_mm_disp = atoms.calc.mmcalc.f_mm_disp
        f_mm_rep = atoms.calc.mmcalc.f_mm_rep
        e_mm_rep = atoms.calc.mmcalc.e_mm_rep 
        e_mm_disp = atoms.calc.mmcalc.e_mm_disp 
        
        ienergy = atoms.calc.ienergy
        immforces = atoms.calc.immforces
        mmforces = atoms.calc.mmforces
        iqmforces = atoms.calc.iqmforces
        pol_dd = atoms.calc.mmcalc.ind_d
        pol_dq = atoms.calc.mmcalc.ind_q
    
    if rank == 0:
        OD[run_no]['qm_idx'] = qmmols
        OD[run_no]['energies'] = {'emono': e_mono, 'etot':ehex, 'eint':eint}
        OD[run_no]['time'] = t_qmmm
        OD[run_no]['positions'] = positions 
        OD[run_no]['qm_positions'] = qm_positions 
        OD[run_no]['mm_positions'] = mm_positions
        OD[run_no]['qm_dipole'] = qm_dipole
        OD[run_no]['forces'] = forces
        ## SCME SPECIFIC
        if hasattr(atoms.calc.mmcalc, 'dpoles'):
            OD[run_no]['mm_dipoles'] = mm_dipoles
            OD[run_no]['mm_dipolesQM'] = mm_dipolesQM
            
            OD[run_no]['mm_qpoles'] = mm_qpoles
            OD[run_no]['mm_qpolesQM'] = mm_qpolesQM
            
            OD[run_no]['scme_forces'] = scme_forces
            OD[run_no]['scme_forcesCM'] = scme_forcesCM
            OD[run_no]['f_mm_disp'] = f_mm_disp
            OD[run_no]['f_mm_rep'] = f_mm_rep
            OD[run_no]['e_mm_rep'] = e_mm_rep
            OD[run_no]['e_mm_disp'] = e_mm_disp
            
            OD[run_no]['ienergy'] = ienergy
            OD[run_no]['immforces'] = immforces
            OD[run_no]['mmforces'] = mmforces
            OD[run_no]['iqmforces'] = iqmforces
            
            OD[run_no]['pol_dd'] = pol_dd
            OD[run_no]['pol_dq'] = pol_dq
            #OD[run_no]['pol_qq'] = pol_qq
        
        np.save(outdictname, OD)
        writeout(path+name+'.dat', numqm=qmct, idx=qmmols,
                 e=(e_mono, ehex, eint, Eoh), 
                 first=first, longest=longest, outnames=outnames) 
    first = False
    step += 1
    atoms.calc = None

### QM
if rank == 0:
    OD = np.load(outdictname).item()
    OD['QM'] = {}
for qq in qm_missing:
    atoms.set_cell(qm_cell)
    e_monos = np.zeros(len(mols))
    qm_monos_dipole = [] 
    if rank == 0:
        OD['QM']['qm_e_monos'] = {}
        OD['QM']['qm_monos_dipole'] = {}
    for n, mol in enumerate(mols):
        mono_kwargs = kwargs.copy()
        cp_mask = np.zeros(len(atoms), bool)
        cp_mask[mol] = True
        qmatoms = atoms[cp_mask]
        if mode.lower() == 'lcao':
            setups = np.array(['paw' if s else 'ghost' for s in cp_mask])
            setups = dict([(j,s) for j, s in enumerate(setups)])
            mono_kwargs['setups'] = setups
            qmatoms = atoms.copy()

        outtxt= path+'outputfiles/'+name+'qm_full_mono_'+str(n).zfill(2)
        qmatoms.set_cell(qm_cell)
        qmatoms.calc = GPAW(txt=outtxt+'.out', **mono_kwargs)
        e_monos[n] = qmatoms.get_potential_energy()
        qm_monos_dipole.append(qmatoms.calc.get_dipole_moment())
       
        if rank == 0:
            OD['QM']['qm_e_monos'][n] = e_monos[n]
            OD['QM']['qm_monos_dipole'][n] = qm_monos_dipole
        qmatoms.calc = None

    setups = {None: 'paw'}
    kwargs['setups'] = setups
    atoms.calc = GPAW(txt=name+'qm_full.out', **kwargs)
    e_mono = sum(e_monos)
    tstart = time.time()
    ehex = atoms.get_potential_energy()
    tend = time.time()
    t_qm = tend - tstart
    eint = ehex - e_mono
    Eoh = 0.
    forces = atoms.get_forces()
    qm_dipole = atoms.calc.get_dipole_moment()
    if rank == 0:
        OD['QM']['energies'] = {'emono': e_mono, 'etot':ehex, 'eint':eint}
        OD['QM']['forces'] = forces 
        OD['QM']['time'] = t_qm
        OD['QM']['qm_dipole'] = qm_dipole
        np.save(outdictname, OD)
        writeout(path+name+'.dat', numqm=nummols, idx=range(len(atoms)), 
                 e=(e_mono, ehex, eint, Eoh), 
                 first=first, longest=longest) 
    atoms.calc = None

### MM
if rank == 0:
    OD = np.load(outdictname).item()
    OD['MM'] = {}
for m in mm_missing:
    atoms.set_cell([100, 100, 100])
    mmatoms = atoms
    atoms.calc = {{ pure_mmcalc|string() }}
    tstart = time.time()
    eint = atoms.get_potential_energy()
    eint = atoms.calc.energy
    tend = time.time()
    t_mm = tend - tstart
    e_mono = 0
    Eoh = 0.
    forces = atoms.calc.forces
    if hasattr(atoms.calc, 'dpoles'):
        dipoles = atoms.calc.dpoles
        qupoles = atoms.calc.qpoles
    if rank == 0:
        OD['MM']['energies'] = {'emono': e_mono, 'etot':eint, 'eint':eint}
        OD['MM']['time'] = t_mm
        OD['MM']['forces'] = forces
        if hasattr(atoms.calc, 'dpoles'):
            OD['MM']['dipoles'] = dipoles
            OD['MM']['qupoles'] = qupoles
        np.save(outdictname, OD)
        writeout(path+name+'.dat', numqm=0, idx=[np.NaN], 
                 e=(e_mono, eint, eint, Eoh), 
                 first=first, longest=longest) 

