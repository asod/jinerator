from ase.units import Hartree, Bohr
from gpaw.eigensolvers import CG, RMMDIIS
import numpy as np
from gpaw import GPAW, Mixer
from ase.io import read

import os
from ase.io.trajectory import TrajectoryWriter
from ase.parallel import rank
try:
    from ase.calculators.qmmm import SCMEEmbedding
    from ase.calculators.scme.calc_qmscme import Calc_QMSCME  # interaction
    from ase.calculators.scme.new_scme_interface import SCME_Interface
except:
    print 'Warning: SCME not loaded'
    pass
from ase.calculators.tip3p import TIP3P
from ase.calculators.tip4p import TIP4P
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions
# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
from pesmakers import set_{{ geom }} as set_geom 


""" Due to cells changing (so grid point positions changing we 
    have to re-calculate BOTH monomer energies at EACH step... 
    Consider reducing your vacuum size a bit... """

def write_out(fpath, data):
    num_column = len(data)
    with open(fpath, 'a') as f:
        f.write(('{:>16.10f}' * num_column).format(*data))
        f.write('\n')


path = '{{ rundir|string() }}'

# Define params
vac = {{ vac }}
kwargs = {{ gpaw_args }}
qmpbc = {{ qmpbc }}  # Hack to help convergence of hybrids etc.

lcaomode = False
if 'mode' in kwargs.keys():
    lcaomode = True


if 'eigensolver' in kwargs:
    kwargs['eigensolver'] = eval(kwargs['eigensolver'])

conf = kwargs['xc']+'h'+str(kwargs['h'])+kwargs['xc']
if lcaomode:
    conf = kwargs['xc']+'h'+str(kwargs['h'])+kwargs['xc']+'_'+kwargs['basis']


movemask = [0, 1, 2]

tag = conf 

points = {{ points }}


the_file = "{{ xyzfile }}"
atoms = read(the_file)

atoms.center(vacuum=vac)
cell = atoms.cell
    

writer = TrajectoryWriter(path+tag+'.traj', 'w', atoms=atoms)
for s, d in enumerate(points):
    set_geom(atoms, d, movemask=movemask)
    atoms.center(vacuum=vac)
    cell = atoms.cell

    e_mono = np.zeros(2)
    for m, mono_idx in enumerate([[0, 1, 2], [3, 4, 5]]):
        mask = np.zeros(len(atoms), bool)
        mask[mono_idx] = True

        mono = atoms[mask]
        setups = {'default':'paw'}
        if lcaomode:  # turn the other mol to ghost
            mono = atoms.copy()  # take whole atoms object
            setups = np.array(['paw' if s else 'ghost' for s in mask])
            setups = dict([(j, s) for j, s in enumerate(setups)])

        mono.cell = cell
        mono.pbc = qmpbc
        mono.calc = GPAW(txt=path + 'outputfiles/'+ tag +'_MONO{0}.out'.format(m),
                         setups=setups, **kwargs)
        try:
            e_mono[m] = mono.get_potential_energy()
        except:
            e_mono[m] = np.NaN

        mono.calc = None

    atoms.calc = GPAW(txt=path+'outputfiles/'+\
                      tag+'_d{0:07.4f}.out'.format(d),
                      **kwargs)
    try:
        e = atoms.get_potential_energy()
    except:
        e = np.NaN
        if rank == 0:
            print('Calculation failed at distance {0:g}!'.format(d))

    data = (d, e, e_mono[0], e_mono[1])

    if rank == 0:
        write_out(path+tag+'.dat', data)

    writer.write(atoms)

    atoms.calc = None
