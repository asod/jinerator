from ase.units import Hartree, Bohr
from gpaw.eigensolvers import CG, RMMDIIS
import numpy as np
from gpaw import GPAW, Mixer
from ase.io import read
import os
from ase.io.trajectory import TrajectoryWriter
from ase.parallel import rank
try:
    from ase.calculators.qmmm import SCMEEmbedding
    from ase.calculators.scme.calc_qmscme import Calc_QMSCME  # interaction
    from ase.calculators.scme.new_scme_interface import SCME_Interface
except:
    print('Warning: SCME not loaded')
    pass
from ase.calculators.tip3p import TIP3P, epsilon0 as eps3, sigma0 as sig3
from ase.calculators.tip4p import TIP4P, epsilon0 as eps4, sigma0 as sig4
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions

# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
from pesmakers import set_{{ geom }} as set_geom 


    
def write_out(fpath, data):
    num_column = len(data)
    with open(fpath, 'a') as f:
        f.write(('{:>16.10f}' * num_column).format(*data))
        f.write('\n')


path = '{{ rundir|string() }}'

# Define params
qmidx = {{ qmidx }}
vac = {{ vac }}
kwargs = {{ gpaw_args }}

if 'eigensolver' in kwargs:
    kwargs['eigensolver'] = eval(kwargs['eigensolver'])

if 0 in qmidx:
    conf = kwargs['xc'] + 'h' + str(kwargs['h']) + '_{{mmcalc|string() }}'
    movemask = [3, 4, 5]
elif 3 in qmidx:
    conf = '{{mmcalc|string() }}_' + kwargs['xc'] + 'h' + str(kwargs['h'])
    movemask = [0, 1, 2]


if hasattr(interaction, 'w_idx'):  # SCME
    parameters = {{ scme_param }}
    g = parameters['damping_value']
    tag = conf + '_g{0:6.4f}'.format(g)

else:
    tag = conf


points = {{ points }}


the_file = "{{ xyzfile }}"
atoms = read(the_file)

atoms.center(vacuum=vac)
cell = atoms.cell
    
# monomer energy:
mono = atoms[qmidx]
mono.cell = cell
mono.calc = GPAW(txt=path + 'outputfiles/'+ tag +'_MONO.out', **kwargs)
try:
    e_mono = mono.get_potential_energy()
except:
    e_mono = np.NaN  # then try to get it otherwise..

writer = TrajectoryWriter(path+tag+'.traj', 'w', atoms=atoms)
e_mm = np.zeros_like(points)


eiqmmm_kwargs = {}
if hasattr(interaction, 'w_idx'):  # SCME
    qm_cell = atoms.get_cell()
    eiqmmm_kwargs['qm_cell'] = qm_cell
    atoms.set_cell([200, 200, 200])

for s, d in enumerate(points):
    set_geom(atoms, d, movemask=movemask)
    
    mmatoms = atoms[movemask]

    emb = {{ embedding|string() }}
    interaction = {{ interaction|string() }}
    atoms.calc = EIQMMM(selection=qmidx,
                        qmcalc=GPAW(txt=path+'outputfiles/'+\
                                    tag+'_d{0:07.4f}.out'.format(d),
                                    **kwargs),
                        mmcalc={{ mmcalc|string() }}{{ mmcalc_args|string()}},
                        interaction=interaction,
                        embedding=emb,
                        output=path + 'outputfiles/' +\
                               tag + '_{0:07.4f}QMMM.log'.format(d),
                               **eiqmmm_kwargs)
    try:
        e = atoms.get_potential_energy()
        if hasattr(interaction, 'w_idx'):  # SCME
            dip = atoms.calc.mmcalc.dpoles[0] + 0.5 * atoms.calc.mmcalc.dpolesQM[0]
        else: 
            dip = np.array([np.NaN, np.NaN, np.NaN])
    except:
        if rank == 0:
            print('Calculation failed at distance {0:g}!'.format(d))
        e = np.NaN  # if not converged 
        dip = np.array([np.NaN, np.NaN, np.NaN])

    data = (d, e, e_mono, dip[0], dip[1], dip[2])

    if rank == 0:
        write_out(path + tag + '.dat', data)

    writer.write(atoms)
