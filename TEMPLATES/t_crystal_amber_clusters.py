import itertools as itools
from ase.units import Hartree, Bohr
import numpy as np
from ase.io import read
# for now just import ALL THE THINGS
from ase.calculators.qmmm import (EIQMMM, Embedding, LJInteractions, 
                                  LJInteractions_Riccardi)
from ase.calculators.crystal import CRYSTAL
import time
from ase.parallel import rank
# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
from waterclusters import (parse_clusterdat, get_nummols, 
                           check_complete, writeout, mm_coulomb)

from ase.calculators.amber import Amber

path = '{{ rundir|string() }}'
MASTER = 0

# define params
vac = {{ vac }}
cryst_args = {{ cryst_args }}
amber_args = {{ mmcalc_args }}

the_file = "{{ xyzfile }}"
atoms = read(the_file)
nummols = len(atoms)/3
tag = the_file.split('/')[-1].split('.xyz')[0]

longest = len(','.join(str(a) for a in range(len(atoms))))

atoms.center(vacuum=vac)
qm_cell = atoms.cell.copy()
atoms.set_cell([100, 100, 100])

first = False
outnames = ['NumQM', 'QMid', 'Emonos', 'Ecluster', 'Eint', 'EmmCoul', 'ClusTime']
mols = [range(i, i+3) for i in range(0, nummols * 3, 3)]  # OHHOHHOHH, ...
name = tag+'_cry_'
### QMMM
data, header, idx = parse_clusterdat(path+name+'.dat')
if not idx:
    nummols = get_nummols(path+'xyz_files/'+the_file)
    missing, qm_missing, mm_missing = check_complete(path+name+'.dat', idx, nummols)
else:
    missing, qm_missing, mm_missing = check_complete(path+name+'.dat', idx)

if (not missing) and (not mm_missing) and (not qm_missing):
    raise ValueError('All data points already collected in: '+path+name+'.dat')

if data is None:  # First run
    first = True
    data = np.zeros((1, 1)) * np.nan  # to get pure qm and pure mm to work


ghostdict = {'O': 92, 'H': 999}

step = 0
n = 3
for qmmols in missing:
    qmct = len(qmmols) / 3
    pair = [qmmols[i:i + n] for i in xrange(0, len(qmmols), n)]
    # make QM subsystem out of ALL QM mols
    mask = np.zeros(len(atoms), bool)
    mask[qmmols] = True
    mmatoms = atoms[~mask]
    mmatoms.calc = {{ mmcalc|string() }}(**amber_args)
    # Get MM coulomb added by crystal
    e_mm_coul = mm_coulomb(mmatoms)
    e_mono = 0
    # get individual monomer energies
    for m, monomol in enumerate(pair):
        qmatoms = atoms[qmmols]

        # COUNTERPOISE: Crystal basis needs ghost atoms with 0 electrons
        # and labels that are mod(label, 100) = 0
        # so hydrogen is 999 and O is 92. Apparently.

        # BUT: Should ONLY be on  OTHER QM atoms.
        cp_mask = np.ones(len(atoms), bool)
        cp_mask[monomol] = False
        cp_mask = cp_mask[qmmols]
        syms = qmatoms.get_chemical_symbols()
        tags = [ghostdict[x] if cp_mask[i] else 0 for i, x in enumerate(syms)]
        qmatoms.set_tags(tags)

        outtxt= path + 'outputfiles/' + name+'numQM' + str(qmct) +\
                'pair_FM_mono_' + str(m).zfill(2)
        qmatoms.set_cell(qm_cell)
        qmatoms.calc = CRYSTAL(**cryst_args) 
        e_mono += qmatoms.get_potential_energy()
        qmatoms.calc = None
        
    ### SPC-f has INTERNAL energies, so they also need to be subtracted!
    e_mm_monos = 0
    mm_mols = np.where(~mask)[0]
    mm_pair = [mm_mols[i:i + n] for i in xrange(0, len(mm_mols), n)]
    for m, monomol in enumerate(mm_pair):
        this_mono = atoms[monomol]
        this_mono.calc = {{ mmcalc|string() }}(**amber_args)
        e_mm_monos += this_mono.get_potential_energy()
        
    interaction = {{ interaction|string() }}

    outtxt = path + 'outputfiles/' + name + 'numQM' +\
             str(qmct) + '_qmmm_qm_FM'
    atoms.calc = EIQMMM(selection=qmmols,
                        qmcalc= CRYSTAL(**cryst_args),
                        mmcalc = {{ mmcalc|string() }}(**amber_args),
                        interaction=interaction,
                        output=outtxt+'QMMM.log',
                        vacuum=None)
    tstart = time.time()
    ehex = atoms.get_potential_energy()
    tstop = time.time()
    elapsed = tstop - tstart
    eint = ehex - e_mono - e_mm_coul - e_mm_monos
    if rank == MASTER:
        writeout(path+name+'.dat', numqm=qmct, idx=qmmols,
                 e=(e_mono, ehex, eint, e_mm_coul, elapsed), 
                 first=first, longest=longest, outnames=outnames) 
    first = False
    step += 1
    atoms.calc = None

### QM
for qq in qm_missing:
    atoms.set_cell(qm_cell)
    e_monos = np.zeros(len(mols))
    e_mm_coul = 0  # doesnt exist when no point charges
    for n,mol in enumerate(mols):
        cp_mask = np.zeros(len(atoms), bool)
        cp_mask[mol] = True
        qmatoms = atoms[cp_mask]
        outtxt= path+'outputfiles/'+name+'qm_full_mono_'+str(n).zfill(2)
        qmatoms.set_cell(qm_cell)
        qmatoms.calc = CRYSTAL(**cryst_args)
        e_monos[n] = qmatoms.get_potential_energy()
        qmatoms.calc = None

    atoms.calc = CRYSTAL(**cryst_args)
    e_mono = sum(e_monos)
    tstart = time.time()
    ehex = atoms.get_potential_energy()
    tstop = time.time()
    elapsed = tstop - tstart
    eint = ehex - e_mono
    if rank == MASTER:
        writeout(path+name+'.dat', numqm=nummols, idx=range(len(atoms)), 
                 e=(e_mono, ehex, eint, e_mm_coul, elapsed), 
                 first=first, longest=longest) 
    atoms.calc = None

### MM
for m in mm_missing:
    atoms.set_cell([100, 100, 100])
    atoms.calc = {{ mmcalc|string() }}(**amber_args)
    tstart = time.time()
    eint = atoms.get_potential_energy()
    tstop = time.time()
    elapsed = tstop - tstart
    ### SPC-f has INTERNAL energies, so they also need to be subtracted!
    e_mm_monos = 0
    mm_mols = range(len(atoms))
    mm_pair = [mm_mols[i:i + n] for i in xrange(0, len(mm_mols), n)]
    for m, monomol in enumerate(mm_pair):
        this_mono = atoms[monomol]
        this_mono.calc = {{ mmcalc|string() }}
        e_mm_monos += this_mono.get_potential_energy()
    if rank == MASTER:
        writeout(path+name+'.dat', numqm=0, idx=[np.NaN], 
                 e=(e_mono, eint, eint, 0, elapsed), 
                 first=first, longest=longest) 

