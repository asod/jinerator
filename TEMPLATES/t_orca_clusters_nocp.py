import itertools as itools
import numpy as np
from ase.io.trajectory import TrajectoryWriter
from ase.io import read
# for now just import ALL THE THINGS
from ase.calculators.tip3p import TIP3P, epsilon0 as eps3, sigma0 as sig3
from ase.calculators.tip4p import TIP4P, epsilon0 as eps4, sigma0 as sig4
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions
from ase.calculators.orca import ORCA
import time
from ase.parallel import rank, broadcast
from os.path import isfile
from os import sep 
# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
from waterclusters import (parse_clusterdat, get_nummols, 
                           check_complete, writeout, make_full_idx)
MASTER = 0
path = '{{ rundir|string() }}'

# define params
vac = {{ vac }}
orca_args = {{ orca_args }}
xc = '{{ xc }}'
basis = '{{ basis }}'


the_file = "{{ xyzfile }}"
atoms = read(the_file)
nummols = len(atoms) // 3
tag = the_file.split('/')[-1].split('.xyz')[0]

longest = len(','.join(str(a) for a in range(len(atoms))))

atoms.center(vacuum=vac)

first = False
outnames = ['NumQM', 'QMid', 'Emonos', 'Ecluster', 'Eint', 'ClusTime']
mols = [range(i, i + 3) for i in range(0, nummols * 3, 3)]  # OHHOHHOHH, ...
name = tag + '_orca_'

data, header, idx = parse_clusterdat(name+'.dat')
if not idx:
    nummols = get_nummols('xyz_files/'+the_file)
    missing, qm_missing, mm_missing = check_complete(name + '.dat', idx, nummols)
else:
    missing, qm_missing, mm_missing = check_complete(name + '.dat', idx)

if (not missing) and (not mm_missing) and (not qm_missing):
    raise ValueError('All data points already collected in: ' + name + '.dat')

if data is None:  # First run
    first = True
    data = np.zeros((1, 1)) * np.nan  # to get pure qm and pure mm to work

outdictname = name + '.npy'
fastqmdictname = name + 'qm_monos.npy'
if not first:
    if rank == 0:
        OD = np.load(path + sep + outdictname, allow_pickle=True, encoding='latin1').item()
        if isfile(fastqmdictname):
            QM = np.load(path + sep + fastqmdictname, allow_pickle=True, encoding='latin1').item()
        else:
            QM = {}
else:
    if rank == 0:
        OD = {}
        OD['JOB_INFO'] = {}
        OD['JOB_INFO']['vac'] = vac
        OD['JOB_INFO']['NAME'] = name
        OD['JOB_INFO']['atoms'] = atoms
        QM = {}

if rank == 0:
    np.save(path + sep + outdictname, OD)
    np.save(path + sep + fastqmdictname, QM)


### QMMM
step = 0
n = 3
full_idx = make_full_idx(nummols)
for qmmols in missing:
    ### Update outdict
    if rank == 0:
        OD = np.load(path + sep + outdictname, allow_pickle=True, encoding='latin1').item()
    run_no = full_idx.index(','.join(str(x) for x in qmmols))
    if rank == 0:
        OD[run_no] = {}

    qmct = len(qmmols) // 3
    pair = [qmmols[i:i + n] for i in range(0, len(qmmols), n)]
    # make QM subsystem out of ALL QM mols
    mask = np.zeros(len(atoms), bool)
    mask[qmmols] = True

    # get individual monomer energies
    e_mono = 0
    if rank == 0:
        OD[run_no]['qmmm_e_monos'] = {}
    for m, monomol in enumerate(pair):
        # This template is without counterpoise correction 
        qmatoms = atoms[monomol] # only look at individual monos

        if rank == 0:
            QM = np.load(path + sep + fastqmdictname, allow_pickle=True, encoding='latin1').item()
        else:
            QM = None
        QM = broadcast(QM)

        if str(monomol) in QM:
            e_this_mono = QM[str(monomol)]
            if rank == 0:
                print(str(monomol)+' single QM energy already in dict, just loading')
        else:
            if rank == 0:
                print(str(monomol)+' single QM energy not in dict, calculating')

            outtxt= name + 'numQM' + str(qmct) +\
                    'pair_FM_mono_' + str(m).zfill(2)
     
            qmatoms.calc = ORCA(label=outtxt,
                                orcasimpleinput=orca_args['orcasimpleinput'],
                                orcablocks=orca_args['orcablocks']) 

            e_this_mono = qmatoms.get_potential_energy()
            QM[str(monomol)] = e_this_mono 

        if rank == 0:
            np.save(path + sep + fastqmdictname, QM)

        e_mono += e_this_mono
        if rank == 0:
            OD[run_no]['qmmm_e_monos'][m] = e_this_mono
        qmatoms.calc = None

    interaction = {{ interaction|string() }}

    outtxt = name + 'numQM' +\
             str(qmct) + '_qmmm_qm_FM'
    atoms.calc = EIQMMM(selection=qmmols,
                        qmcalc=ORCA(label=outtxt,
                                    orcasimpleinput=orca_args['orcasimpleinput'],
                                    orcablocks=orca_args['orcablocks']),
                        mmcalc = {{mmcalc}}(rc=np.inf),
                        interaction=interaction,
                        output=outtxt+'QMMM.log',
                        vacuum=None)
    tstart = time.time()
    ehex = atoms.get_potential_energy()
    tstop = time.time()
    elapsed = tstop - tstart
    eint = ehex - e_mono

    ### SAVE STUFF TO OUTPUT DICT -- seems like it only wants to live on 0
    positions = atoms.get_positions()
    qm_positions = atoms[qmmols].get_positions()
    mm_positions = atoms[~mask].get_positions()
    
    ## New things from branch ASE-Sep18Fix
    if rank == MASTER:
        OD[run_no]['qm_idx'] = qmmols
        OD[run_no]['energies'] = {'emono': e_mono, 'etot':ehex, 'eint':eint}
        OD[run_no]['time'] = elapsed
        OD[run_no]['positions'] = positions 
        OD[run_no]['qm_positions'] = qm_positions 
        OD[run_no]['mm_positions'] = mm_positions
        np.save(path + sep + outdictname, OD)
        writeout(path + name + '.dat', numqm=qmct, idx=qmmols,
                 e=(e_mono, ehex, eint, elapsed), 
                 first=first, longest=longest, outnames=outnames) 
    first = False
    step += 1
    atoms.calc = None

### QM
if rank == 0:
    OD = np.load(path + sep + outdictname, allow_pickle=True, encoding='latin1').item()
    OD['QM'] = {}
atoms.set_tags(np.zeros(len(atoms)))
for qq in qm_missing:
    e_monos = np.zeros(len(mols))
    if rank == 0:
        OD['QM']['qm_e_monos'] = {}
    for n, mol in enumerate(mols):
        mono = atoms[mol]
        
        outtxt= name + 'qm_full_mono_' + str(n).zfill(2)
        mono.calc = ORCA(label=outtxt,
                            orcasimpleinput=orca_args['orcasimpleinput'],
                            orcablocks=orca_args['orcablocks'])
        e_monos[n] = mono.get_potential_energy()

        if rank == 0:
            OD['QM']['qm_e_monos'][n] = e_monos[n]
        mono.calc = None

    outtxt= name + 'qm_full'
    atoms.calc = ORCA(label=outtxt,
                      orcasimpleinput=orca_args['orcasimpleinput'],
                      orcablocks=orca_args['orcablocks'])

    e_mono = sum(e_monos)
    tstart = time.time()
    ehex = atoms.get_potential_energy()
    tstop = time.time()
    elapsed = tstop - tstart
    eint = ehex - e_mono
    if rank == MASTER:
        OD['QM']['energies'] = {'emono': e_mono, 'etot':ehex, 'eint':eint}
        OD['QM']['time'] = elapsed
        np.save(path + sep + outdictname, OD)
        writeout(path + name + '.dat', numqm=nummols, idx=range(len(atoms)), 
                 e=(e_mono, ehex, eint, elapsed), 
                 first=first, longest=longest) 
    atoms.calc = None

### MM
if rank == 0:
    OD = np.load(path + sep + outdictname, allow_pickle=True, encoding='latin1').item()
    OD['MM'] = {}
for m in mm_missing:
    atoms.calc = {{mmcalc}}(rc=np.inf)
    tstart = time.time()
    eint = atoms.get_potential_energy()
    tstop = time.time()
    elapsed = tstop - tstart
    e_mono = 0
    if rank == MASTER:
        OD['MM']['energies'] = {'emono': e_mono, 'etot':eint, 'eint':eint}
        OD['MM']['time'] = elapsed
        np.save(path + sep + outdictname, OD)
        writeout(path + name + '.dat', numqm=0, idx=[np.NaN], 
                 e=(e_mono, eint, eint,  elapsed), 
                 first=first, longest=longest) 

