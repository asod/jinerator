import numpy as np
import os
from ase.calculators.orca import ORCA
from ase.io import read
from ase.io.trajectory import TrajectoryWriter
from ase.parallel import rank
from ase.units import Hartree, Bohr
# add my own tools
import sys
sys.path.append('{{ rootdir|string() }}')
from pesmakers import set_{{ geom }} as set_geom 


def write_out(fpath, data):
    num_column = len(data)
    with open(fpath, 'a') as f:
        f.write(('{:>18.10f}' * num_column).format(*data))
        f.write('\n')


# Define params
vac = {{ vac }}
kwargs = {{ orca_args }}
xc = '{{ xc }}'
basis = '{{ basis }}'

conf = xc + '_' + basis
movemask = [0, 1, 2]
tag = conf 

points = {{ points }}

the_file = "{{ xyzfile }}"
atoms = read(the_file)

atoms.center(vacuum=vac)
cell = atoms.cell
    
writer = TrajectoryWriter(tag+'.traj', 'w', atoms=atoms)
for s, d in enumerate(points):
    set_geom(atoms, d, movemask=movemask)
    atoms.center(vacuum=vac)
    cell = atoms.cell

    e_mono = np.zeros(2)
    for m, mono_idx in enumerate([[0, 1, 2], [3, 4, 5]]):
        tags = np.zeros(len(atoms))
        tags[mono_idx] = 71  # This means GHOST for orca

        # turn the other mol to ghost
        mono = atoms.copy()  # take whole atoms object
        mono.set_tags(tags)

        mono.calc = ORCA(label=tag + 'mono_{0:d}'.format(m),
                         orcasimpleinput=kwargs['orcasimpleinput'],
                         orcablocks=kwargs['orcablocks'])
                    
        try:
            e_mono[m] = mono.get_potential_energy()
        except:
            e_mono[m] = np.NaN

        mono.calc = None

    atoms.calc = ORCA(label=tag,
                      orcasimpleinput=kwargs['orcasimpleinput'],
                      orcablocks=kwargs['orcablocks'])
                 
    try:
        e = atoms.get_potential_energy()
    except:
        e = np.NaN
        if rank == 0:
            print('Calculation failed at distance {0:g}!'.format(d))

    data = (d, e, e_mono[0], e_mono[1])

    if rank == 0:
        write_out(tag+'.dat', data)

    writer.write(atoms)

    atoms.calc = None
