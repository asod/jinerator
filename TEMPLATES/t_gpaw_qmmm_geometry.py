import numpy as np
from gpaw import  GPAW
from gpaw.eigensolvers import CG, RMMDIIS
from gpaw.cluster import Cluster
from ase.constraints import FixBondLengths
from ase.io import Trajectory
from ase.calculators.qmmm import EIQMMM, Embedding, LJInteractions
from ase.calculators.tip3p import TIP3P, epsilon0 as eps3, sigma0 as sig3
from ase.calculators.tip4p import TIP4P, epsilon0 as eps4, sigma0 as sig4
from ase.optimize.fire import FIRE
try:  # SCME
    from ase.calculators.qmmm import SCMEEmbedding
    from ase.calculators.scme.calc_qmscme import Calc_QMSCME
    from ase.calculators.scme.new_scme_interface import SCME_Interface
except:
    pass

import sys
sys.path.append('{{ rootdir|string() }}')

path = '{{ rundir|string() }}' + 'outputfiles/'

qmidx = [{{ qmidx }}]
the_file = "{{ xyzfile }}"
vac = {{ vac }}
kwargs = {{ gpaw_args }}

tag = the_file.split('/')[-1].split('.xyz')[0]
tag += '{{ extra }}'


atoms = read(the_file)

# need to make exact same h for each geom
cluster = Cluster(Atoms(atoms.get_chemical_symbols(),
                        atoms.get_positions()))
cluster.minimal_box(vac, kwargs['h'])
cell = cluster.get_cell()
atoms.set_cell(cell)
atoms.center()

mask = np.zeros(len(atoms), bool)
mask[qmidx] = True
mmidx = np.where(~mask)[0]

# fix all bonds
c1 = FixBondLengths([(3 * i + j, 3 * i + (j + 1) % 3)
                     for i in range(len(atoms) // 3)
                     for j in [0, 1, 2]])

atoms.constraints = c1
# Save what is QM using the tags
tags = np.zeros(len(atoms))
tags[mask] = 1
atoms.set_tags(tags)

emb = {{ embedding|string() }}
interaction = {{ interaction|string() }}

eiqmmm_kwargs = {}
opt_kwargs = {}

if hasattr(interaction, 'w_idx'):  # SCME
    qm_cell = atoms.get_cell()
    eiqmmm_kwargs['qm_cell'] = qm_cell
    atoms.set_cell([200, 200, 200])

    opt_kwargs['force_consistent'] = False


atoms.calc = EIQMMM(selection=qmidx,
                    qmcalc=GPAW(txt=path + tag + '.out', **kwargs),
                    mmcalc={{ mmcalc|string() }}{{ mmcalc_args|string()}}, 
                    interaction=interaction,
                    embedding=emb,
                    output= path + tag + '_QMMM.log',
                    **eiqmmm_kwargs)

opt = FIRE(atoms, logfile=path + tag + '_opt.log', **opt_kwargs)
traj = Trajectory(path + tag + '.traj', 'w', atoms)
opt.attach(traj.write, interval=1)
opt.run(fmax={{ fmax }}, steps={{ geom_steps }})

