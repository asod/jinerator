**Templating and Analysis Tools for Water QM/MM Test- and Benchmarks.**  
To be used with ASE: https://gitlab.com/ase/ase


**The Templater:**  
1. Sets/Chooses a template file based on the chosen benchmark type (see below)  
2. Updates the 'megadict' with input parameters  
3. Finds the relevant structure files for the calculations  
4. Fills the templates and writes the ASE-input files   


**Types of benchmarks & tests:**  
1. PES scans of relevant water dimer geometries. **calctype=pes**  
    1a: O-O Distance: pestype='roo'  
    1b: Scanning H-accepting lone pair rotation: pestype='alpha'  
    1c: OOH angle: pestype='ooh'  
    1d: Acceptor dihedral: pestype='hoox'  
    1f: Donor dihedral: pestype='ooxh'  
2. Scanning the DFT/SCME multipole damping factor for any of the above PESs  
3. Water Cluster Interaction energy differences **calctype=WaterClusters**  
4. (Hexamer) Geometry optimiztions. **calctype=Geometry**  


**Currently supported QM codes:**  
1. GPAW  
2. CRYSTAL  
3. ORCA  (via the ase-branch: https://gitlab.com/asod/ase/tree/orca)


**Currently supported MM codes (all through ASE):**  
1. ASE native calculators  
2. SCME   
3. Amber (very limited support)  



**Analysis Tools:**  
     1. Angular Distributions (ADFs) and Hydrogen Bonding    


**WIPS:**  
* [x] Add and test Hexamer geometry benchmark 
* [x] EIQMMM uses qmpbc=False (always), get rid of that.  
* [x] Re-generalize the t_gpaw_scme2019_clusters_grid.py template to work with non-SCME MM calcs (needs testing)
* [x] Generalize the above template to include CP correction for LCAO mode  (needs testing)
* [x] Re-generalize t_gpaw_pes.py to work with non-SCME MM calcs  (needs testing)
* [x] EIQMMM uses qm_cell, which is not in trunk (not sure all templates are tested)
* [ ] Move files to a more standard location-scheme: sqrtiny/tools, sqrtiny/jinerator.py etc..   
* [ ] Homogenize some silly design choices, e.g.: init vs in set_defaults()  
* [ ] refactor EIQMMM inputs into **kwargs style for more flexibility  
* [ ] make sure multipole convergence criteria wont show up for EE calcs  
* [ ] The functionality of the toolbox has grown a lot since its inception, so refactoring jinerator.py should be done

**REQUIRED DEPENDENCIES:**  
**jinja2:** For the templating. `pip install Jinja2`  
**ASE:**    https://wiki.fysik.dtu.dk/ase/  


**OPTIONAL DEPENDENCIES:**  
**MPI4PY:** For simple parallelization over trajectory files of ADF sampling.  
	`pip install mpi4py ` 
	

**RMSD:** https://pypi.org/project/rmsd/ (for multipole analysis of DFT/SCME benchmarks, and geometry analysis of hexamers)  


Check the EXAMPLES/ for examples. Run them from root dir with:  


`python -m EXAMPLES.<python-file>` (no .py)  

